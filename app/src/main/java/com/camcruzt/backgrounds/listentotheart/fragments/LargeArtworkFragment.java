package com.camcruzt.backgrounds.listentotheart.fragments;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.transition.TransitionInflater;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.camcruzt.backgrounds.listentotheart.R;
import com.camcruzt.backgrounds.listentotheart.activities.MainActivity;
import com.camcruzt.backgrounds.listentotheart.databinding.FragmentArtworkLargeImageBinding;
import com.camcruzt.backgrounds.listentotheart.model.Artwork;
import com.camcruzt.backgrounds.listentotheart.model.Report;
import com.camcruzt.backgrounds.listentotheart.model.User;
import com.camcruzt.backgrounds.listentotheart.utils.Utils;
import com.camcruzt.backgrounds.listentotheart.viewmodel.ArtworkViewModel;
import com.camcruzt.backgrounds.listentotheart.viewmodel.UserViewModel;
import com.camcruzt.backgrounds.listentotheart.widget.ArtworkService;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static com.camcruzt.backgrounds.listentotheart.fragments.DetailArtworkFragment.SAVED_ARTWORK;
import static com.camcruzt.backgrounds.listentotheart.fragments.ProfileFragment.PARCELABLE_KEY;
import static com.camcruzt.backgrounds.listentotheart.fragments.ShareFragment.HORIZONTAL_KEY;

public class LargeArtworkFragment extends BaseFragment {

    public static final String USER_PARCELABLE_KEY = "userKey";
    private static final String SAVED_USER = "SavedUser";
    private static final String INTENT_ARTWORK = "mIntentArtwork";
    private Artwork mArtwork;
    private FragmentArtworkLargeImageBinding mBinding;
    private android.support.v7.app.ActionBar supportActionBar;
    private AHBottomNavigation bottomNavigation;
    private InterstitialAd mInterstitialAd;
    private User mAuthorUser;
    private Artwork mIntentArtwork;
    private AdRequest mAdRequest;
    private AlertDialog alertDialog;
    private CharSequence[] dialogOptions;
    private String mSignedUserId;
    private String reportContent = "";
    private DatabaseReference mReportDatabaseReference;
    private Report report;


    public LargeArtworkFragment() {
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(SAVED_ARTWORK, mArtwork);
        outState.putParcelable(SAVED_USER, mAuthorUser);
        outState.putParcelable(INTENT_ARTWORK, mIntentArtwork);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*mAdRequest = new AdRequest.Builder()
                .addTestDevice("E04EC52520C747F61516FD226CC32D0F")
                .build();
        requestNewInterstitial();
        mInterstitialAd.loadAd(mAdRequest);*/

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        mSignedUserId = sharedPref.getString(getString(R.string.user_id_key_sharedpreferences), "");
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_artwork_large_image, container, false);
        View rootView = mBinding.getRoot();
        supportActionBar = ((MainActivity) getActivity()).getSupportActionBar();
        bottomNavigation = (AHBottomNavigation) getActivity().findViewById(R.id.bottom_navigation);
        UserViewModel userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);

        FirebaseDatabase firebaseDatabase = ((MainActivity) getActivity()).mFirebaseDatabase;
        mReportDatabaseReference = firebaseDatabase.getReference().child("reports");

        if (searchView != null) {
            setSearchViewVisibility(false);
        }

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(PARCELABLE_KEY)) {
            mIntentArtwork = (Artwork) bundle.getParcelable(PARCELABLE_KEY);
            ((MainActivity) getActivity()).updateToolbarTitle(mIntentArtwork.getTitle());
            LiveData<User> userLiveData = userViewModel.getUserById(mIntentArtwork.getAuthorId());
            ArtworkViewModel artworkViewModel = ViewModelProviders.of(this).get(ArtworkViewModel.class);
            LiveData<Artwork> artworkLiveData = artworkViewModel.getDataSnapshotLiveData(mIntentArtwork.getId());
            artworkLiveData.observe(this, new Observer<Artwork>() {
                @Override
                public void onChanged(@Nullable Artwork artwork) {
                    mArtwork = artwork;
                    if (isAdded() && getActivity() != null) {
                        returnAuthorUser(userLiveData);
                    }

                    mBinding.infoButton.setOnClickListener(v -> {
                        if (mAuthorUser != null) {
                            switchToDetailFragment(mArtwork, mAuthorUser);
                        }
                    });

                    mBinding.expandFullscreenButton.setOnClickListener(v -> {
                        switchToFullscreenFragment(mArtwork);
                        /*if (mInterstitialAd.isLoaded()) {
                            mInterstitialAd.show();
                        } else {
                            Log.d("TAG", "The interstitial wasn't loaded yet.");
                        }*/

                    });

                }
            });

            /*mInterstitialAd.setAdListener(new AdListener(){
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    mInterstitialAd.loadAd(new AdRequest.Builder().build());
                    switchToFullscreenFragment(mArtwork);
                }
            });*/
        }

        mBinding.topBar.usernameMainScreenTv.setOnClickListener(v -> {
            if (mAuthorUser != null) {
                showUserProfile(mAuthorUser, null);
            }
        });

        mBinding.topBar.reportButton.setOnClickListener(v -> {
           getActivity().openContextMenu(v);
        });
        registerForContextMenu(mBinding.topBar.reportButton);
        if (isAdded()){
            dialogOptions = getResources().getTextArray(R.array.inappropriate_content);
        }

        Artwork artworkWidget = new Artwork(mIntentArtwork.getAuthorUsername(), mIntentArtwork.getTitle(),
                mIntentArtwork.getYear(), mIntentArtwork.getImageUrl());

        ArtworkService.startActionUpdateWidgetArtwork(getActivity(), artworkWidget);
        return rootView;
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if(isAdded()){
            getActivity().getMenuInflater().inflate(R.menu.artwork_menu, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.reportOption) {
            showReportDialog(mArtwork.getId());
            return true;
        }
        return super.onContextItemSelected(item);
    }

    private void showReportDialog(String artworkId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getContext().getResources().getString(R.string.report_dialog_title));
        builder.setSingleChoiceItems(dialogOptions, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch(item)
                {
                    case 0:
                        reportContent = "Sex";
                        break;
                    case 1:
                        reportContent = "Violence";
                        break;
                    case 2:
                        reportContent = "Bullying";
                        break;
                    case 3:
                        reportContent = "Drugs";
                        break;
                }
                report = new Report(mSignedUserId, artworkId, reportContent);
            }
        });

        builder.setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if(reportContent != null){
                    String databaseKey = mReportDatabaseReference.push().getKey();
                    mReportDatabaseReference.child(databaseKey).setValue(report);
                    Toast toast = Toast.makeText(getContext(), getContext().getResources().getString(R.string.thanks_message), Toast.LENGTH_LONG);
                    LinearLayout layout = (LinearLayout) toast.getView();
                    if (layout.getChildCount() > 0) {
                        TextView tv = (TextView) layout.getChildAt(0);
                        tv.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
                    }
                    toast.show();
                }
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        alertDialog = builder.create();
        alertDialog.show();
    }

    private void returnAuthorUser(LiveData<User> userLiveData) {
        userLiveData.observe(this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                if (user != null) {
                    mAuthorUser = user;

                    RequestOptions userOptions = new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .override(150, 150)
                            .circleCrop()
                            .error(R.drawable.free_user_icon_21);

                    Glide.with(getContext())
                            .load(mAuthorUser.getPhotoUrl())
                            .apply(userOptions)
                            .into(mBinding.topBar.profilePhotoMainScreen);
                    updateUI();
                }
            }
        });
    }

    private void updateUI() {
        if (mAuthorUser != null) {
            mBinding.topBar.usernameMainScreenTv.setText(mAuthorUser.getUsername());
        }
        String dateString = Utils.timestampToDateString(mArtwork.getTimestampCreatedLong());
        mBinding.topBar.timestampMainScreenTv.setText(dateString);
        //String title = mArtwork.getTitle() + ", " + mArtwork.getYear();
        mBinding.artworkTitleTv.setText(mArtwork.getTitle());
        mBinding.artworkAuthorTv.setText(getString(R.string.author_comma, mArtwork.getAuthorUsername()));
        mBinding.artworkDateTv.setText(mArtwork.getYear());
        if (mArtwork.getMainAudioUrl() != null && mArtwork.getMainAudioUrl().startsWith("http") ||
                mArtwork.getDetailAudioUrl() != null && mArtwork.getDetailAudioUrl().startsWith("http")) {
            mBinding.expandFullscreenButton.setImageResource(R.drawable.speaker_filled);
        } else {
            mBinding.expandFullscreenButton.setImageResource(R.drawable.expand_icon);
        }

        RequestOptions options;
        if (mArtwork.getImageOrientation().equals(HORIZONTAL_KEY)) {
            options = new RequestOptions()
                    .fitCenter()
                    .override(1080, 950)
                    .placeholder(new ColorDrawable(Color.WHITE))
                    .error(R.drawable.free_user_icon_21);
        } else {
            options = new RequestOptions()
                    .centerCrop()
                    .override(1080, 950)
                    .placeholder(new ColorDrawable(Color.WHITE))
                    .error(R.drawable.free_user_icon_21);
        }

        Glide.with(this)
                .load(mArtwork.getThumbnailUrl())
                .apply(options)
                .into(mBinding.artworkLargeIv);

    }

    public void switchToDetailFragment(@NonNull Artwork artwork, @NonNull User user) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(PARCELABLE_KEY, artwork);
        bundle.putParcelable(USER_PARCELABLE_KEY, user);
        DetailArtworkFragment detailArtworkFragment = new DetailArtworkFragment();
        detailArtworkFragment.setArguments(bundle);

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager
                .beginTransaction()
                .replace(R.id.content_frame, detailArtworkFragment)
                .addToBackStack(null)
                .commit();
    }

    public void switchToFullscreenFragment(Artwork artwork) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(PARCELABLE_KEY, artwork);
        FullscreenArtworkFragment fullscreenArtworkFragment = new FullscreenArtworkFragment();
        fullscreenArtworkFragment.setArguments(bundle);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.setExitTransition(TransitionInflater.from(getActivity()).inflateTransition(R.transition.fade));
            fullscreenArtworkFragment.setEnterTransition(TransitionInflater.from(getActivity()).inflateTransition(R.transition.fade));
        }
        ((MainActivity) getActivity()).replaceFragment(fullscreenArtworkFragment, null);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!supportActionBar.isShowing()) {
            supportActionBar.show();
            bottomNavigation.setVisibility(View.VISIBLE);
            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }

    private void requestNewInterstitial() {
        mInterstitialAd = new InterstitialAd(getContext());
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_id));
        mInterstitialAd.loadAd(mAdRequest);
    }
}