package com.camcruzt.backgrounds.listentotheart.fragments;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.MediaController;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.camcruzt.backgrounds.listentotheart.R;
import com.camcruzt.backgrounds.listentotheart.activities.MainActivity;
import com.camcruzt.backgrounds.listentotheart.audio.AudioController;
import com.camcruzt.backgrounds.listentotheart.audio.AudioPlayerService;
import com.camcruzt.backgrounds.listentotheart.audio.AudioPlayerService.AudioBinder;
import com.camcruzt.backgrounds.listentotheart.databinding.FullscreenArtworkBinding;
import com.camcruzt.backgrounds.listentotheart.model.Artwork;
import com.github.piasy.biv.BigImageViewer;
import com.github.piasy.biv.loader.glide.GlideImageLoader;
import com.github.piasy.biv.view.BigImageView;

import java.util.ArrayList;

import static com.camcruzt.backgrounds.listentotheart.fragments.ProfileFragment.PARCELABLE_KEY;

//Media controller based on
// https://code.tutsplus.com/tutorials/create-a-music-player-on-android-user-controls--mobile-22787
public class FullscreenArtworkFragment extends Fragment implements MediaController.MediaPlayerControl {

    private static final String AUDIO_POSITION_KEY = "audioPositionKey";
    private static final String TRACK_SELECTED_KEY = "trackSelectedKey";
    private FullscreenArtworkBinding mBinding;
    private AudioPlayerService musicSrv;
    private Intent playIntent;
    private boolean musicBound=false;
    private AudioController mController;
    private ArrayList<String> audios = new ArrayList<>();
    private boolean paused=false, playbackPaused=false;
    private int audioPos = 0;
    private int currentTrack = 0;
    private boolean thereIsAudio = false;

    public FullscreenArtworkFragment() {
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if(thereIsAudio){
            outState.putInt(AUDIO_POSITION_KEY, audioPos);
            outState.putInt(TRACK_SELECTED_KEY, musicSrv.getTrackSelected());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        BigImageViewer.initialize(GlideImageLoader.with(getContext()));
        mBinding = DataBindingUtil.inflate(inflater,
                R.layout.fullscreen_artwork, container, false);

        updateLayout();
        Artwork artwork = new Artwork();
        View rootView = mBinding.getRoot();
        Bundle bundle = getArguments();
        if(bundle!=null && bundle.containsKey(PARCELABLE_KEY)){
            artwork = (Artwork) bundle.getParcelable(PARCELABLE_KEY);
        }

        if(artwork.getMainAudioUrl()!=null && artwork.getMainAudioUrl().startsWith("http")){
            audios.add(artwork.getMainAudioUrl());
        }

        if(artwork.getDetailAudioUrl()!=null && artwork.getDetailAudioUrl().startsWith("http")){
            audios.add(artwork.getDetailAudioUrl());
        }

        if(audios.size()>0){
            thereIsAudio = true;
        }

        if(savedInstanceState != null && !savedInstanceState.isEmpty()) {
            audioPos = savedInstanceState.getInt(AUDIO_POSITION_KEY);
            currentTrack = savedInstanceState.getInt(TRACK_SELECTED_KEY);
        }

        mBinding.mBigImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mController !=null){
                    if(!mController.isShowing() ){
                        mController.show();
                    } else {
                        mController.hide();
                    }
                }
            }
        });

        mBinding.mBigImage.setInitScaleType(BigImageView.INIT_SCALE_TYPE_CENTER_CROP);
        mBinding.mBigImage.showImage(Uri.parse(artwork.getImageUrl()));
        return rootView;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setController() {
        mController = new AudioController(getContext());

        if(audios.size()>1){
            mController.setPrevNextListeners(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    playNext();
                }
            }, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    playPrev();
                }
            });
        }

        mController.setMediaPlayer(this);
        mController.setAnchorView(mBinding.mBigImage);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void playNext(){
        musicSrv.playNext();
        if(playbackPaused){
            setController();
            audioPos=0;
            playbackPaused=false;
        }
        mController.show(0);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void playPrev(){
        musicSrv.playPrev();
        if(playbackPaused){
            setController();
            audioPos=0;
            playbackPaused=false;
        }
        mController.show(0);
    }

    private ServiceConnection musicConnection = new ServiceConnection(){
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            AudioBinder binder = (AudioBinder)service;
            musicSrv = binder.getService();
            musicSrv.setAudioList(audios);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    musicBound = true;
                    musicSrv.setInitPosition(audioPos);
                    audioPos = 0;
                    musicSrv.setTrackSelected(currentTrack);
                    musicSrv.playAudio();
                    mController.show();
                }
            },450);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            musicBound = false;
        }
    };

    protected void updateLayout() {
        android.support.v7.app.ActionBar supportActionBar = ((MainActivity) getActivity()).getSupportActionBar();
        AHBottomNavigation bottomNavigation = (AHBottomNavigation) getActivity().findViewById(R.id.bottom_navigation);
        supportActionBar.hide();
        bottomNavigation.setVisibility(View.GONE);
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onStart() {
        super.onStart();
        if(thereIsAudio){
            setController();
            if(playIntent==null){
                playIntent = new Intent(getActivity(), AudioPlayerService.class);
                getActivity().bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
                getActivity().startService(playIntent);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        pause();
        if(musicSrv!=null){
            audioPos=musicSrv.getPos();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onResume() {
        super.onResume();
        if(thereIsAudio){
            if(paused){
                setController();
                paused=false;
            }
        }
    }

    @Override
    public void onStop() {
        if(mController !=null){
            mController.hide();
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(musicBound) {
            musicSrv.stopPlaying();
            getActivity().unbindService(musicConnection);
            getActivity().stopService(playIntent);
            musicSrv = null;
        }
    }

    @Override
    public void start() {
        if(!playbackPaused){
            musicSrv.setInitPosition(audioPos);
            audioPos = 0;
            musicSrv.setTrackSelected(currentTrack);
            musicSrv.playAudio();
        } else {
                musicSrv.seek(audioPos);
                musicSrv.start();
                playbackPaused=false;
        }
    }

    @Override
    public void pause() {
        playbackPaused=true;
        if(musicSrv!=null){
            audioPos=musicSrv.getPos();
            musicSrv.pausePlayer();
        }
    }

    @Override
    public int getDuration() {
        if(musicSrv!=null && musicBound && musicSrv.isPlaying()){
            return musicSrv.getDuration();
        } else {
            return 0;
        }
    }

    @Override
    public int getCurrentPosition() {
        if(musicSrv!=null && musicBound){
            return musicSrv.getPos();
        }
        return 0;
    }

    @Override
    public void seekTo(int pos) {
        musicSrv.seek(pos);
    }

    @Override
    public boolean isPlaying() {
        if(musicSrv!=null && musicBound){
            return musicSrv.isPlaying();
        }
        return false;
    }

    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getAudioSessionId() {
        return 0;
    }
}