package com.camcruzt.backgrounds.listentotheart.activities;

import android.app.ActivityOptions;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.camcruzt.backgrounds.listentotheart.R;
import com.camcruzt.backgrounds.listentotheart.databinding.ActivityMainBinding;
import com.camcruzt.backgrounds.listentotheart.fragments.AboutFragment;
import com.camcruzt.backgrounds.listentotheart.fragments.ContactFragment;
import com.camcruzt.backgrounds.listentotheart.fragments.DetailArtworkFragment;
import com.camcruzt.backgrounds.listentotheart.fragments.EditProfileFragment;
import com.camcruzt.backgrounds.listentotheart.fragments.FullscreenArtworkFragment;
import com.camcruzt.backgrounds.listentotheart.fragments.HomeFragment;
import com.camcruzt.backgrounds.listentotheart.fragments.LargeArtworkFragment;
import com.camcruzt.backgrounds.listentotheart.fragments.PrivacyFragment;
import com.camcruzt.backgrounds.listentotheart.fragments.ProfileFragment;
import com.camcruzt.backgrounds.listentotheart.fragments.SearchFragment;
import com.camcruzt.backgrounds.listentotheart.fragments.ShareFragment;
import com.camcruzt.backgrounds.listentotheart.fragments.TermsFragment;
import com.camcruzt.backgrounds.listentotheart.model.User;
import com.camcruzt.backgrounds.listentotheart.utils.Utils;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.Arrays;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    public static final String USER_ID_KEY = "userUid";
    public static final String HOME_FRAGMENT_TAG = "home_fragment_tag";
    public static final String ANONYMOUS = "anonymous";
    private static final String HOME_INIT_KEY = "homeInit";
    private static final String SEARCH_FRAGMENT_TAG = "search_fragment_tag";
    private static final String SHARE_FRAGMENT_TAG = "share_fragment_tag";
    public static final String PROFILE_FRAGMENT_TAG = "profile_fragment_tag";
    private static final String NAV_DRAWER_FRAGMENT_TAG = "nav_fragment";
    public FirebaseDatabase mFirebaseDatabase;
    public DatabaseReference mUsersDatabaseReference;
    private ValueEventListener mUsersValueEventListener;
    public FirebaseStorage mFirebaseStorage;
    public StorageReference mProfilePhotos;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private static final int RC_SIGN_IN = 1;
    public String mFullName, mUserEmail, mCurrentUserUid;
    public User mCurrentUserInDatabase;
    public FirebaseUser mCurrentFirebaseUser;
    private Fragment savedFragment;
    private static final String SAVED_FRAGMENT = "savedFragment";
    private AHBottomNavigation bottomNavigation;
    private Boolean isHomeFragmentInitialized = false;
    private boolean returnPreviousFragment = false;
    private static final String TAG = MainActivity.class.getName();
    private SharedPreferences sharedPref;
    private ActivityMainBinding mBinding;
    private String previousFragmentTAG;
    private Fragment fragment;
    private String lastFragmentTAG;
    private ActionBarDrawerToggle mDrawerToggle;
    private ActionBar mActionBar;

    public String mAuthorDefaultProfileImageURL = "https://firebasestorage.googleapis.com/v0/b/listen-to-the-art.appspot.com/o/free-user-icon-21.jpg?alt=media&token=fe99603e-ac30-4147-b5e4-790067064e76";

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(USER_ID_KEY, mCurrentUserUid);
        outState.putBoolean(HOME_INIT_KEY, isHomeFragmentInitialized);
        savedFragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (savedFragment != null) {
            getSupportFragmentManager().putFragment(outState, SAVED_FRAGMENT, savedFragment);
        }

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        isHomeFragmentInitialized = savedInstanceState.getBoolean(HOME_INIT_KEY);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this,
                R.layout.activity_main);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        setSupportActionBar(mBinding.mainLayout.toolbar);
        mActionBar = getSupportActionBar();

        if(mActionBar!=null){
            mActionBar.setHomeAsUpIndicator(R.drawable.nav_icon_white);
        }

        mDrawerToggle = new ActionBarDrawerToggle(this,
                mBinding.drawerLayout, mBinding.mainLayout.toolbar, R.string.open_drawertoggle,R.string.closed_drawer);
        mDrawerToggle.syncState();
        mActionBar.setDisplayHomeAsUpEnabled(true);

        MobileAds.initialize(getApplicationContext(), getString(R.string.admob_id));
        if (!Utils.isOnline(this)) {
            mBinding.mainLayout.noInternetTv.setVisibility(View.VISIBLE);
            updateToolbarTitle(getString(R.string.app_name));
        } else {
            setBottomNavigation();
            //Initialize Firebase components
            mFirebaseDatabase = FirebaseDatabase.getInstance();
            mFirebaseAuth = FirebaseAuth.getInstance();
            mFirebaseStorage = FirebaseStorage.getInstance();

            mUsersDatabaseReference = mFirebaseDatabase.getReference().child("users");
            mProfilePhotos = mFirebaseStorage.getReference().child("profile_photos");

            mAuthStateListener = new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                    mCurrentFirebaseUser = firebaseAuth.getCurrentUser();
                    if (mCurrentFirebaseUser != null) {
                        // User is signed in
                        onSignedInInitialize(mCurrentFirebaseUser.getDisplayName(), mCurrentFirebaseUser.getEmail());
                        mCurrentUserUid = mCurrentFirebaseUser.getUid();
                        sharedPref.edit().putString(getString(R.string.user_id_key_sharedpreferences),
                                mCurrentFirebaseUser.getUid()).apply();
                        if (bottomNavigation.getCurrentItem() == 0 && !isHomeFragmentInitialized) {
                            bottomNavigation.setCurrentItem(0);
                        }
                    } else {
                        onSignedOutCleanup();
                        startActivityForResult(
                                // Get an instance of AuthUI based on the default app
                                AuthUI.getInstance()
                                        .createSignInIntentBuilder()
                                        .setIsSmartLockEnabled(false)
                                        .setLogo(R.drawable.logo_txt_margin)
                                        .setAvailableProviders(Arrays.asList(
                                                new AuthUI.IdpConfig.EmailBuilder().build(),
                                                new AuthUI.IdpConfig.GoogleBuilder().build()))

                                        .build(), RC_SIGN_IN);
                    }
                }
            };

            bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
                @Override
                public boolean onTabSelected(int position, boolean wasSelected) {
                    Fragment fragment;
                    switch (position) {
                        case 0:
                            fragment = new HomeFragment();
                            replaceFragment(fragment, HOME_FRAGMENT_TAG);
                            break;
                        case 1:
                            fragment = new SearchFragment();
                            replaceFragment(fragment, SEARCH_FRAGMENT_TAG);
                            break;
                        case 2:
                            fragment = new ShareFragment();
                            replaceFragment(fragment, SHARE_FRAGMENT_TAG);
                            break;
                        case 3:
                            fragment = new ProfileFragment();
                            replaceFragment(fragment, PROFILE_FRAGMENT_TAG);
                            break;
                    }
                    return true;
                }
            });

            mBinding.navview.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    fragment = null;
                    switch (menuItem.getItemId()) {
                        case R.id.menu_about_backgrounds:
                            fragment = new AboutFragment();
                            mBinding.drawerLayout.closeDrawer(mBinding.navview);
                            new Handler().postDelayed(() ->
                                    replaceFragment(fragment, NAV_DRAWER_FRAGMENT_TAG),
                                    280);
                            break;
                        case R.id.menu_about_us:
                            mBinding.drawerLayout.closeDrawer(mBinding.navview);
                            new Handler().postDelayed(() -> openAboutUsActivity(), 280);
                            break;
                        case R.id.menu_terms:
                            fragment = new TermsFragment();
                            mBinding.drawerLayout.closeDrawer(mBinding.navview);
                            new Handler().postDelayed(() ->
                                    replaceFragment(fragment, NAV_DRAWER_FRAGMENT_TAG),
                                    280);
                            break;
                        case R.id.menu_privacy:
                            fragment = new PrivacyFragment();
                            mBinding.drawerLayout.closeDrawer(mBinding.navview);
                            new Handler().postDelayed(() ->
                                    replaceFragment(fragment, NAV_DRAWER_FRAGMENT_TAG),
                                    300);
                            break;
                        case R.id.menu_contact:
                            fragment = new ContactFragment();
                            mBinding.drawerLayout.closeDrawer(mBinding.navview);
                            replaceFragment(fragment, NAV_DRAWER_FRAGMENT_TAG);
                            break;
                        case R.id.menu_sign_out:
                            mBinding.drawerLayout.closeDrawer(mBinding.navview);
                            AuthUI.getInstance().signOut(getBaseContext());
                            break;
                    }

                    return false;
                }
            });

            if (savedInstanceState != null) {
                isHomeFragmentInitialized = savedInstanceState.getBoolean(HOME_INIT_KEY);

                if (savedFragment instanceof HomeFragment) {
                    savedFragment = (HomeFragment) getSupportFragmentManager().getFragment(savedInstanceState, SAVED_FRAGMENT);
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, savedFragment).commit();
                }
                if (savedFragment instanceof SearchFragment) {
                    savedFragment = (SearchFragment) getSupportFragmentManager().getFragment(savedInstanceState, SAVED_FRAGMENT);
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, savedFragment).commit();
                }

                if (savedFragment instanceof ProfileFragment) {
                    savedFragment = (ProfileFragment) getSupportFragmentManager().getFragment(savedInstanceState, SAVED_FRAGMENT);
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, savedFragment).commit();
                }

                if (savedFragment instanceof DetailArtworkFragment) {
                    savedFragment = (DetailArtworkFragment) getSupportFragmentManager().getFragment(savedInstanceState, SAVED_FRAGMENT);
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, savedFragment).commit();
                }

                if (savedFragment instanceof FullscreenArtworkFragment) {
                    savedFragment = (FullscreenArtworkFragment) getSupportFragmentManager().getFragment(savedInstanceState, SAVED_FRAGMENT);
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, savedFragment).commit();
                }

                if (savedFragment instanceof LargeArtworkFragment) {
                    savedFragment = (LargeArtworkFragment) getSupportFragmentManager().getFragment(savedInstanceState, SAVED_FRAGMENT);
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, savedFragment).commit();
                }

            }
        }

        getSupportFragmentManager().addOnBackStackChangedListener(
                new FragmentManager.OnBackStackChangedListener() {
                    public void onBackStackChanged() {

                        int entryCount = getSupportFragmentManager().getBackStackEntryCount();
                        if (entryCount >= 1) {
                            lastFragmentTAG = getSupportFragmentManager().getBackStackEntryAt(entryCount - 1).getName();
                            if (entryCount == 1) {
                                previousFragmentTAG = HOME_FRAGMENT_TAG;
                            } else {
                                previousFragmentTAG = getSupportFragmentManager().getBackStackEntryAt(entryCount - 2).getName();
                            }
                        }
                        setHomeIndicator(entryCount);
                    }
                });
    }

    private void setHomeIndicator(int entryCount) {
        //If there's a name, we're on a root fragment, otherwise, is a nested fragment.
        if (lastFragmentTAG != null || entryCount == 0) {
            mActionBar.setDisplayHomeAsUpEnabled(false);
            mDrawerToggle.setDrawerIndicatorEnabled(false);
            mActionBar.setHomeAsUpIndicator(R.drawable.nav_icon_white);
            mDrawerToggle.setDrawerIndicatorEnabled(true);
        } else {
            mDrawerToggle.setDrawerIndicatorEnabled(false);
            mActionBar.setDisplayHomeAsUpEnabled(false);
            mActionBar.setDisplayHomeAsUpEnabled(true);
            mActionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);

            mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }


    private void setBottomNavigation() {
        bottomNavigation = mBinding.mainLayout.bottomNavigation;

        AHBottomNavigationItem item1 = new
                AHBottomNavigationItem(R.string.home, R.drawable.home_icon, R.color.white);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.search, R.drawable.search_icon, R.color.white);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.share, R.drawable.share_icon, R.color.white);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.profile, R.drawable.profile_icon, R.color.white);

        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);
        bottomNavigation.addItem(item4);

        bottomNavigation.setCurrentItem(0);
        bottomNavigation.setDefaultBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
        bottomNavigation.setAccentColor(getResources().getColor(R.color.colorSecondaryDark));
        bottomNavigation.setInactiveColor(getResources().getColor(R.color.white));
        bottomNavigation.setBehaviorTranslationEnabled(false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mFirebaseAuth != null) {
            mFirebaseAuth.removeAuthStateListener(mAuthStateListener);
        }

        if(mUsersDatabaseReference != null && mUsersValueEventListener != null){
            mUsersDatabaseReference.removeEventListener(mUsersValueEventListener);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mFirebaseAuth != null) {
            mFirebaseAuth.addAuthStateListener(mAuthStateListener);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void onSignedInInitialize(String username, String email) {
        mFullName = username;
        mUserEmail = email;
        queryAndSaveNewUser();
    }

    private void onSignedOutCleanup() {
        mFullName = ANONYMOUS;
        sharedPref.edit().putBoolean(getString(R.string.just_logged_in_shared_preferences), true).apply();
        sharedPref.edit().putString(getString(R.string.user_id_key_sharedpreferences),
                "").apply();
        sharedPref.edit().putInt(getString(R.string.order_criteria_key_sharedpreferences),
                0).apply();
        detachUsersValueListener();
    }

    private void queryAndSaveNewUser() {
        //Taken from: https://stackoverflow.com/questions/38948905/
        mUsersValueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    //email exists in Database
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        mCurrentUserInDatabase = postSnapshot.getValue(User.class);
                        Log.e("Main user exists", mCurrentUserInDatabase.toString());
                    }
                } else {
                    //email doesn't exists.
                    if (mCurrentFirebaseUser != null) {
                        String[] username = mUserEmail.split("@");
                        HashMap<String, String> followedAccounts = new HashMap<>();
                        followedAccounts.put(mCurrentFirebaseUser.getUid(), "true");
                        String sampleUserId = "lpjkWH7qVMUQOX4TNLf5VeNx4Sb2";
                        followedAccounts.put(sampleUserId, "true");

                        User user = new User(mCurrentFirebaseUser.getUid(), mUserEmail, "",
                                mFullName, username[0], "", "", "",
                                mAuthorDefaultProfileImageURL, "0", false, followedAccounts);
                        mUsersDatabaseReference.child(mCurrentFirebaseUser.getUid()).setValue(user);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        };
        mUsersDatabaseReference.orderByChild("email").equalTo(mUserEmail)
                .addValueEventListener(mUsersValueEventListener);
    }

    private void detachUsersValueListener() {
        if (mUsersValueEventListener != null) {
            mUsersDatabaseReference.removeEventListener(mUsersValueEventListener);
            mUsersValueEventListener = null;
            mUsersDatabaseReference.setValue(null);
        }
    }

    public void updateToolbarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        DialogInterface.OnClickListener discardButtonClickListener;

        if (fragment instanceof EditProfileFragment && !returnPreviousFragment) {
            fragment = (EditProfileFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
            boolean mProfileHasChanged = ((EditProfileFragment) fragment).mProfileHasChanged;
            if (mProfileHasChanged) {
                discardButtonClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        returnPreviousFragment = true;
                        onBackPressed();
                    }
                };
                showUnsavedChangesDialog(discardButtonClickListener);
            } else {
                returnPreviousFragment = true;
                onBackPressed();
            }
        } else if (fragment instanceof ShareFragment && !returnPreviousFragment) {
            fragment = (ShareFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
            boolean mArtworkHasChanged = ((ShareFragment) fragment).mArtworkHasChanged;
            if (mArtworkHasChanged) {
                discardButtonClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        returnPreviousFragment = true;
                        onBackPressed();
                    }
                };
                showUnsavedChangesDialog(discardButtonClickListener);
            } else {
                returnPreviousFragment = true;
                onBackPressed();
            }
        } else if (previousFragmentTAG != null && !returnPreviousFragment) {
            switch (previousFragmentTAG) {
                case HOME_FRAGMENT_TAG:
                    bottomNavigation.setCurrentItem(0, false);
                    break;
                case SEARCH_FRAGMENT_TAG:
                    bottomNavigation.setCurrentItem(1, false);
                    break;
                case SHARE_FRAGMENT_TAG:
                    bottomNavigation.setCurrentItem(2, false);
                    break;
                case PROFILE_FRAGMENT_TAG:
                    bottomNavigation.setCurrentItem(3, false);
                    break;
            }
            returnPreviousFragment = true;
            onBackPressed();
        } else {
            returnPreviousFragment = false;
            super.onBackPressed();
        }
    }

    private void showUnsavedChangesDialog
            (DialogInterface.OnClickListener discardButtonClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.unsaved_changes_dialog_msg);
        builder.setPositiveButton(R.string.discard, discardButtonClickListener);
        builder.setNegativeButton(R.string.keep_editing, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void replaceFragment(Fragment fragment, String fragmentTAG) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.content_frame, fragment);
        if (isHomeFragmentInitialized) {
            transaction.addToBackStack(fragmentTAG);
        }
        transaction.commitAllowingStateLoss();
        if (!isHomeFragmentInitialized) {
            isHomeFragmentInitialized = true;
        }
    }

    private void openAboutUsActivity() {
        Intent intent = new Intent(this, AboutActivity.class);
        Bundle bundle = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            bundle = ActivityOptions
                    .makeSceneTransitionAnimation(this)
                    .toBundle();
        }
        startActivity(intent, bundle);
    }
}