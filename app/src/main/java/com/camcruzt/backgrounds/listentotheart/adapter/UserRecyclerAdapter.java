package com.camcruzt.backgrounds.listentotheart.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.camcruzt.backgrounds.listentotheart.R;
import com.camcruzt.backgrounds.listentotheart.model.User;

import java.util.ArrayList;
import java.util.List;

import static com.camcruzt.backgrounds.listentotheart.fragments.ProfileFragment.DEF_PACKAGE;

public class UserRecyclerAdapter extends RecyclerView.Adapter<UserRecyclerAdapter.UserViewHolder> {

    private Context context;
    private ArrayList<User> mUsers;
    final private ListItemClickListener mOnClickListener;

    public UserRecyclerAdapter(Context context, ArrayList<User> users, ListItemClickListener mOnClickListener) {
        this.context = context;
        this.mUsers = users;
        this.mOnClickListener = mOnClickListener;
    }

    class UserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView userImageView;
        ImageView countryFlag;
        TextView name;
        TextView country;
        TextView about;
        public UserViewHolder(@NonNull View itemView) {
            super(itemView);
            userImageView = (ImageView)itemView.findViewById(R.id.found_user_image);
            countryFlag = (ImageView)itemView.findViewById(R.id.flag_item_people);
            name = (TextView)itemView.findViewById(R.id.found_user_name);
            country = (TextView)itemView.findViewById(R.id.found_user_country);
            about = (TextView)itemView.findViewById(R.id.found_user_about);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            mOnClickListener.onUserClick(clickedPosition);
        }
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        int userItem = R.layout.item_search_people;
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(userItem, viewGroup, false);
        UserViewHolder viewHolder = new UserViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        User currentUser = mUsers.get(position);

        RequestOptions options = new RequestOptions()
                .circleCrop()
                .override(650,650)
                .error(R.drawable.free_user_icon_21);

        Glide.with(context)
                .load(currentUser.getPhotoUrl())
                .apply(options)
                .into(holder.userImageView);


        if(currentUser.getCountry()!=null){
            String lowerCaseCountry = currentUser.getCountry().toLowerCase();
            int idFlagDrawable = context.getResources().getIdentifier("flag_" + lowerCaseCountry,
                    "drawable", DEF_PACKAGE);
            holder.country.setText(currentUser.getCountry());
            holder.countryFlag.setImageResource(idFlagDrawable);
        }

        if(currentUser.getUsername()!=null){
            holder.name.setText(currentUser.getUsername());
        } else {
            holder.name.setText(currentUser.getName());
        }

        holder.about.setText(currentUser.getAboutMe());
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    public void setData(List<User> data) {
        if (data != null && !data.isEmpty()) {
            mUsers.clear();
            mUsers.addAll(data);
            notifyDataSetChanged();
        }
    }

    public Boolean hasBeenAdded(String userId){
        boolean userHasBeenAdded = false;
        for (int i = 0; i< mUsers.size(); i++){
            if (mUsers.get(i).getId().equals(userId)){
                userHasBeenAdded = true;
            }
        }
        return userHasBeenAdded;
    }

    public void add(User user){
        mUsers.add(user);
        notifyDataSetChanged();
    }

    public void clear() {
        int size = getItemCount();
        if (size > 0) {
            mUsers.clear();
            notifyDataSetChanged();
        }
    }

    public interface ListItemClickListener {
        void onUserClick(int clickedItemIndex);
    }
}