package com.camcruzt.backgrounds.listentotheart.fragments;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.camcruzt.backgrounds.listentotheart.R;
import com.camcruzt.backgrounds.listentotheart.activities.MainActivity;
import com.camcruzt.backgrounds.listentotheart.databinding.FragmentArtworkDetailsBinding;
import com.camcruzt.backgrounds.listentotheart.model.Artwork;
import com.camcruzt.backgrounds.listentotheart.model.Tag;
import com.camcruzt.backgrounds.listentotheart.model.User;
import com.camcruzt.backgrounds.listentotheart.viewmodel.ArtworkViewModel;
import com.camcruzt.backgrounds.listentotheart.viewmodel.UserViewModel;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.plumillonforge.android.chipview.Chip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.camcruzt.backgrounds.listentotheart.fragments.LargeArtworkFragment.USER_PARCELABLE_KEY;
import static com.camcruzt.backgrounds.listentotheart.fragments.ProfileFragment.PARCELABLE_KEY;
import static com.camcruzt.backgrounds.listentotheart.fragments.ShareFragment.HORIZONTAL_KEY;
import static com.camcruzt.backgrounds.listentotheart.utils.Utils.EXAMPLE_URL;

public class DetailArtworkFragment extends BaseFragment {

    public static final String SAVED_ARTWORK = "savedArtwork";

    private ExpandableTextView artworkDescription;
    private ExpandableTextView additionalInfo;
    private FragmentArtworkDetailsBinding mBinding;
    private Artwork mArtwork;
    private User authorUser;
    public FirebaseStorage mFirebaseStorage;
    private String TAG = DetailArtworkFragment.class.getName();
    private List<Chip> mChipList = new ArrayList<>();
    private boolean isCurrentUserOwner = true;
    private ArtworkViewModel mArtworkViewModel;
    private UserViewModel userViewModel;
    private android.support.v7.app.ActionBar supportActionBar;
    private AHBottomNavigation bottomNavigation;
    private String mSignedUserId;
    private SharedPreferences sharedPref;
    private Artwork mIntentArtwork;
    private AdRequest mAdRequest;
    private InterstitialAd mInterstitialAd;

    public DetailArtworkFragment() {
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(SAVED_ARTWORK, mArtwork);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        mArtworkViewModel = ViewModelProviders.of(this).get(ArtworkViewModel.class);
        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        mSignedUserId = sharedPref.getString(getString(R.string.user_id_key_sharedpreferences), "");

        /*mAdRequest = new AdRequest.Builder()
                .addTestDevice("E04EC52520C747F61516FD226CC32D0F")
                .build();
        requestNewInterstitial();
        mInterstitialAd.loadAd(mAdRequest);*/
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!supportActionBar.isShowing()) {
            supportActionBar.show();
            bottomNavigation.setVisibility(View.VISIBLE);
            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_artwork_details, container, false);
        View rootView = mBinding.getRoot();

        artworkDescription = (ExpandableTextView) rootView.findViewById(R.id.expand_text_view);
        additionalInfo = (ExpandableTextView) rootView.findViewById(R.id.expandableAdditionalInfoTextView);
        supportActionBar = ((MainActivity) getActivity()).getSupportActionBar();
        bottomNavigation = (AHBottomNavigation) getActivity().findViewById(R.id.bottom_navigation);

        if (searchView != null) {
            setSearchViewVisibility(false);
        }

        Bundle bundle = getArguments();
        mIntentArtwork = (Artwork) bundle.getParcelable(PARCELABLE_KEY);
        authorUser = (User) bundle.getParcelable(USER_PARCELABLE_KEY);

        ArtworkViewModel artworkViewModel = ViewModelProviders.of(this).get(ArtworkViewModel.class);
        LiveData<Artwork> artworkLiveData = artworkViewModel.getDataSnapshotLiveData(mIntentArtwork.getId());
        artworkLiveData.observe(this, new Observer<Artwork>() {
            @Override
            public void onChanged(@Nullable Artwork artwork) {
                mArtwork = artwork;
                if (mArtwork != null) {
                    updateUI();
                }

                mBinding.artworkLayout.expandFullscreenButton.setOnClickListener(v -> {
                    switchToFullscreenFragment(mArtwork);
                    //interstitial is not implemented

                    /*if (mArtwork != null) {
                        if (mInterstitialAd.isLoaded()) {
                            mInterstitialAd.show();
                        } else {
                            Log.d("TAG", "The interstitial wasn't loaded yet.");
                        }
                    }*/
                });
            }
        });
        mFirebaseStorage = ViewModelProviders.of(this).get(ArtworkViewModel.class).mFirebaseStorage;


        if (isAdded() && !mSignedUserId.equals(mIntentArtwork.getAuthorId())) {
            getActivity().invalidateOptionsMenu();
            isCurrentUserOwner = false;
        }

        if (authorUser == null) {
            LiveData<User> userLiveData = userViewModel.getUserById(mIntentArtwork.getAuthorId());
            returnAuthorUser(userLiveData);
        }

        mBinding.artworkLayout.detailCloseBtn.setOnClickListener(v -> {
            getActivity().getSupportFragmentManager().popBackStack();
        });

        if (isAdded()) {
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice("E04EC52520C747F61516FD226CC32D0F")
                    .build();
            mBinding.detailFragmentadView.loadAd(adRequest);
        }

        return rootView;
    }

    private void returnAuthorUser(LiveData<User> userLiveData) {
        userLiveData.observe(getActivity(), new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                authorUser = user;
            }
        });
    }

    private void updateUI() {
        ((MainActivity) getActivity()).updateToolbarTitle(mArtwork.getTitle());
        mBinding.artworkLayout.artworkTitleTv.setText(getString(R.string.author_comma, mArtwork.getTitle()));
        mBinding.artworkLayout.artworkDateTv.setText(mArtwork.getYear());
        mBinding.artworkLayout.mediumTv.setText(mArtwork.getMedium());
        String dimensions = mArtwork.getWidth() + " cm " + " x " + mArtwork.getHeight() + " cm ";
        mBinding.artworkLayout.dimensionsTv.setText(dimensions);
        artworkDescription.setText(mArtwork.getDescription());

        if (!mArtwork.getAdditionalInfo().isEmpty()) {
            TextView detailLabel = (TextView) mBinding.getRoot().findViewById(R.id.additionalInfoLabelTextView);
            detailLabel.setVisibility(View.VISIBLE);
            additionalInfo.setText(mArtwork.getAdditionalInfo());
            additionalInfo.setVisibility(View.VISIBLE);
        }

        if (mArtwork.getKeywords() != null && mArtwork.getKeywords().size() > 0) {
            HashMap<String, String> keywordsMap = mArtwork.getKeywords();
            if (keywordsMap.size() != mChipList.size()) {
                for (Map.Entry<String, String> keywords : mArtwork.getKeywords().entrySet()) {
                    String key = keywords.getKey();
                    mChipList.add(new Tag(key));
                }
            }
            mBinding.artworkLayout.keywordsLabelTv.setVisibility(View.VISIBLE);
            mBinding.artworkLayout.viewArtworkChipview.setChipList(mChipList);
        }

        if (mArtwork.getMainAudioUrl() != null && mArtwork.getMainAudioUrl().startsWith("http") ||
                mArtwork.getDetailAudioUrl() != null && mArtwork.getDetailAudioUrl().startsWith("http")) {
            mBinding.artworkLayout.expandFullscreenButton.setImageResource(R.drawable.speaker_filled);
        } else {
            mBinding.artworkLayout.expandFullscreenButton.setImageResource(R.drawable.expand_icon);
        }

        RequestOptions options;
        if (mArtwork.getImageOrientation().equals(HORIZONTAL_KEY)) {
            options = new RequestOptions()
                    .fitCenter()
                    .override(800, 650)
                    .placeholder(new ColorDrawable(Color.WHITE))
                    .error(R.drawable.free_user_icon_21);
        } else {
            options = new RequestOptions()
                    .centerCrop()
                    .override(800, 650)
                    .placeholder(new ColorDrawable(Color.WHITE))
                    .error(R.drawable.free_user_icon_21);
        }

        Glide.with(this)
                .load(mArtwork.getThumbnailUrl())
                .apply(options)
                .into(mBinding.artworkSmallTv);
    }

    private void showDeleteConfirmationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(R.string.delete_dialog_msg);
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                if (mArtwork.getImageUrl() != null && !mArtwork.getImageUrl().equals(EXAMPLE_URL)) {
                    deleteImageOnStorage(mArtwork.getImageUrl());
                } else {
                    deleteArtworkOnDB();
                }
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Glide.with(this).clear(mBinding.artworkSmallTv);
    }

    private void deleteImageOnStorage(String imageUrl) {
        StorageReference photoRef = mFirebaseStorage.getReferenceFromUrl(imageUrl);
        photoRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // File deleted successfully
                Log.d(TAG, "onSuccess: deleted file");
                if (mArtwork.getThumbnailUrl() != null && !mArtwork.getThumbnailUrl().isEmpty()) {
                    deleteAudioFromStorage(mArtwork.getThumbnailUrl());
                }

                if (mArtwork.getMainAudioUrl() != null && !mArtwork.getMainAudioUrl().isEmpty()) {
                    deleteAudioFromStorage(mArtwork.getMainAudioUrl());
                }

                if (mArtwork.getDetailAudioUrl() != null && !mArtwork.getDetailAudioUrl().isEmpty()) {
                    deleteAudioFromStorage(mArtwork.getDetailAudioUrl());
                }
                deleteArtworkOnDB();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d(TAG, "onFailure: did not delete file");
            }
        });
    }

    private void deleteAudioFromStorage(String audioUrl) {
        StorageReference photoRef = mFirebaseStorage.getReferenceFromUrl(audioUrl);
        photoRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // File deleted successfully
                Log.v(TAG, "Audio " + audioUrl + "deleted!");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // An error occurred!
                Log.e(TAG, "onFailure: did not delete file");
            }
        });
    }

    private void deleteArtworkOnDB() {
        mArtworkViewModel.deleteArtworkFromDb(mArtwork.getId());
        mArtworkViewModel.getDeleteArtworkIsSuccessful().observe(this, wasSuccessful -> {
            if (wasSuccessful) {
                Toast.makeText(getContext(), getString(R.string.delete_message), Toast.LENGTH_SHORT).show();
                decreasePublicationCounter();
                returnToProfileScreen(2);
            } else {
                Toast.makeText(getContext(), getString(R.string.error_deleting), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void decreasePublicationCounter() {
        UserViewModel userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        int numberPublications = Integer.parseInt(((MainActivity) getActivity())
                .mCurrentUserInDatabase.getNumberPublications());
        userViewModel.updateCounter
                (((MainActivity) getActivity()).mCurrentUserUid, numberPublications - 1);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.detail_menu, menu);
    }

    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.editItemBtn);
        MenuItem item2 = menu.findItem(R.id.deleteItemBtn);
        if (!isCurrentUserOwner) {
            item.setVisible(false);
            item2.setVisible(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.editItemBtn:
                switchToEditArtworkFragment(mArtwork);
                return true;
            case R.id.deleteItemBtn:
                showDeleteConfirmationDialog();
                return true;
            case R.id.sign_out_menu:
                AuthUI.getInstance().signOut(getContext());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void switchToEditArtworkFragment(Artwork artwork) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(PARCELABLE_KEY, artwork);
        ShareFragment editArtwork = new ShareFragment();
        editArtwork.setArguments(bundle);

        ((MainActivity) getActivity()).replaceFragment(editArtwork, null);
    }

    public void switchToFullscreenFragment(Artwork artwork) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(PARCELABLE_KEY, artwork);
        FullscreenArtworkFragment fullscreenArtworkFragment = new FullscreenArtworkFragment();
        fullscreenArtworkFragment.setArguments(bundle);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.setExitTransition(TransitionInflater.from(getActivity()).inflateTransition(R.transition.fade));
            fullscreenArtworkFragment.setEnterTransition(TransitionInflater.from(getActivity()).inflateTransition(R.transition.fade));
        }

        ((MainActivity) getActivity()).replaceFragment(fullscreenArtworkFragment, null);
    }

    private void requestNewInterstitial() {
        mInterstitialAd = new InterstitialAd(getContext());
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_id));
        mInterstitialAd.loadAd(mAdRequest);
    }
}