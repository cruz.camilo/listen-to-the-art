package com.camcruzt.backgrounds.listentotheart.viewmodel;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.camcruzt.backgrounds.listentotheart.livedata.FirebaseQueryLiveData;
import com.camcruzt.backgrounds.listentotheart.model.Artwork;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class ArtworkViewModel extends ViewModel {

    private static String TAG = ArtworkViewModel.class.getName();
    public DatabaseReference dataRef =
            FirebaseDatabase.getInstance().getReference().child("artworks");
    private static StorageReference mArtworksStorageReference =
            FirebaseStorage.getInstance().getReference().child("artworks");
    public FirebaseStorage mFirebaseStorage = FirebaseStorage.getInstance();
    private List<Artwork> mList = new ArrayList<>();

    private FirebaseQueryLiveData liveData;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @NonNull
    public LiveData<Artwork> getDataSnapshotLiveData(@NonNull String uId) {

        liveData = new FirebaseQueryLiveData(dataRef.child(uId));
        LiveData<Artwork> artworkLiveData = Transformations.map(liveData, new SingleDeserializer());

        return artworkLiveData;
    }

    @NonNull
    public LiveData<List<Artwork>> getArtworkListLiveData(@NonNull Query query){
        liveData = new FirebaseQueryLiveData(query);
        LiveData<List<Artwork>> artworkList =
                Transformations.map(liveData, new Deserializer());
        return artworkList;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private class SingleDeserializer implements Function<DataSnapshot, Artwork> {
        @Override
        public Artwork apply(DataSnapshot dataSnapshot) {
            return dataSnapshot.getValue(Artwork.class);
        }
    }

    private class Deserializer implements Function<DataSnapshot, List<Artwork>> {
        @Override
        public List<Artwork> apply(DataSnapshot dataSnapshot) {
            mList.clear();
            for(DataSnapshot snap : dataSnapshot.getChildren()){
                Artwork artwork = snap.getValue(Artwork.class);
                mList.add(artwork);
            }
            return mList;
        }
    }

    private final MutableLiveData<Boolean> artworkDeleteWasSuccessful = new MutableLiveData<>();
    public MutableLiveData<Boolean> getDeleteArtworkIsSuccessful(){
        return artworkDeleteWasSuccessful;
    }

    public void deleteArtworkFromDb(String artworkId){
        DatabaseReference artworkRef = dataRef.child(artworkId);

        Task<Void> deleteTask = artworkRef.removeValue();
        deleteTask.addOnCompleteListener(task -> artworkDeleteWasSuccessful.setValue(true));
        deleteTask.addOnFailureListener(e -> {
            artworkDeleteWasSuccessful.setValue(false);
            Log.e(TAG, e.toString());
        });
    }

    public void updateArtwork(String artworkId, Map<String, Object> artworkUpdates,
                              Map<String, String> keywords){
        DatabaseReference artworkRef = dataRef.child(artworkId);
        DatabaseReference keywordsRef = dataRef.child(artworkId).child("keywords");
        artworkRef.updateChildren(artworkUpdates);
        if(keywords!=null){
            keywordsRef.setValue(keywords);
        }
    }

    public void saveAudioToDb(String artworkId, Map<String, Object> audioUpdates){
        //Taken from https://stackoverflow.com/questions/49101324/
        DatabaseReference artworkRef = dataRef.child(artworkId);
        artworkRef.updateChildren(audioUpdates);
    }

    private String mFileUrl = "";

    private final MutableLiveData<Boolean> pictureUploadIsSuccessful = new MutableLiveData<>();

    public void setFileUrl(String fileUrl){
        mFileUrl = fileUrl;
    }

    public String getFileUrl(){
        return mFileUrl;
    }


    private final MutableLiveData<Boolean> fileUploadWasSuccessful = new MutableLiveData<>();

    public MutableLiveData<Boolean> getFileUploadIsSuccessful(){
        return fileUploadWasSuccessful;
    }

    public void uploadFileToDb(Uri fileUri, String artworkId, String filename, String userId, Boolean isAudio){
        StorageReference fileRef;
        if(!isAudio){
            if (filename == null){
                filename = fileUri.getLastPathSegment();
            }
            fileRef = mArtworksStorageReference.child(userId).child(artworkId).child(filename);
        } else {
            fileRef = mArtworksStorageReference.child(userId).child(artworkId).child("audio").child(filename);
        }

        UploadTask uploadTask = fileRef.putFile(fileUri);
        //Taken from https://github.com/udacity/and-nd-firebase/issues/41
        uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>()  {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                return fileRef.getDownloadUrl();
            }
        }).addOnCompleteListener(task -> {
            mFileUrl = task.getResult().toString();
            fileUploadWasSuccessful.setValue(true);
        });

        uploadTask.addOnFailureListener(e -> {
            fileUploadWasSuccessful.setValue(false);
            Log.e("MODEL ERROR", e.toString());
        });
    }

    /*public void saveArtworkToDataBase(String uId, Artwork artwork, Map<String, String> keywords ){
        DatabaseReference artworkRef = dataRef.child(uId);
        String databaseKey = artworkRef.push().getKey();

        Task saveArtworkTask = artworkRef
                .child(databaseKey)
                .setValue(artwork);

        artworkRef.child(databaseKey).child("keywords").setValue(keywords);
        //saveArtworkTask.addOnSuccessListener(o -> messageUploadIsSuccessful.setValue(true));
    }*/
}


