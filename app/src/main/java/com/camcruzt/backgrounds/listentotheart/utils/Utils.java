package com.camcruzt.backgrounds.listentotheart.utils;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.camcruzt.backgrounds.listentotheart.model.Artwork;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import okhttp3.internal.Util;

public class Utils {

    private static final String sampleDescription = "The Advanced Android Development course is intended for experienced " +
            "developers who have Java programming experience and know the fundamentals of how to " +
            "build an Android app using the Java language. The course is offered as an in-person " +
            "course at selected colleges, facilitated by college faculty";

    private static final String TAG = Util.class.getName();
    public static final String EXAMPLE_URL = "https://developers.google.com/training/courses/images/ailt-splash.png";
    private static final String defaultNoProfileImage = "https://firebasestorage.googleapis.com/v0/b/listen-to-the-art.appspot.com/o/free-user-icon-21.jpg?alt=media&token=653af218-33ff-4bfb-ad82-64617062c234";

    //Taken from: https://stackoverflow.com/questions/20197487/
    public static String getFilePath(Uri uri, Context context) {
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri,
                filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String filePath = cursor.getString(columnIndex);

        cursor.close();
        return filePath;
    }

    public static String timestampToDateString(Long timestamp){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        String date = simpleDateFormat.format(new Date(timestamp));
                //getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT).format(new Date(timestamp));
        return date;
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static Artwork createMockArtwork(String artworkId, String authorId){
        Artwork mockArtwork = new Artwork(artworkId, authorId, "janedoe",
                defaultNoProfileImage,
                "Android Developer", "2019",
                "45", "45", "Sample",
                "https://developers.google.com/training/courses/images/ailt-splash.png",
                "https://developers.google.com/training/courses/images/ailt-splash.png",
                sampleDescription, "Mock", "Horizontal");
        return mockArtwork;
    }

    //Taken from https://www.baeldung.com/java-string-title-case
    public static String convertToTitleCaseIteratingChars(String text) {
        if (text == null || text.isEmpty()) {
            return text;
        }

        StringBuilder converted = new StringBuilder();

        boolean convertNext = true;
        for (char ch : text.toCharArray()) {
            if (Character.isSpaceChar(ch)) {
                convertNext = true;
            } else if (convertNext) {
                ch = Character.toTitleCase(ch);
                convertNext = false;
            } else {
                ch = Character.toLowerCase(ch);
            }
            converted.append(ch);
        }

        return converted.toString();
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    // Method to get the bitmap resized
    public static Bitmap getBitmapFromUri(Uri uri, int targetPhotoW, int targetPhotoH, Context context) {

        if (uri == null || uri.toString().isEmpty())
            return null;

        Log.v("Target size", String.valueOf(targetPhotoW) + " " + String.valueOf(targetPhotoH));

        InputStream input = null;
        try {
            input = context.getContentResolver().openInputStream(uri);

            // Get the dimensions of the bitmap
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(input, null, bmOptions);
            input.close();

            int photoW = bmOptions.outWidth;
            int photoH = bmOptions.outHeight;

            Log.v("Photo size", String.valueOf(photoW) + " " + String.valueOf(photoH));

            // Determine how much to scale down the image
            int scaleFactor = Math.min(photoW / targetPhotoW, photoH / targetPhotoH);

            // Decode the image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor;
            bmOptions.inPurgeable = true;

            input = context.getContentResolver().openInputStream(uri);
            Bitmap bitmap = BitmapFactory.decodeStream(input, null, bmOptions);
            input.close();
            return bitmap;

        } catch (FileNotFoundException fne) {
            Log.e(TAG, "Failed to load image.", fne);
            return null;
        } catch (Exception e) {
            Log.e(TAG, "Failed to load image.", e);
            return null;
        } finally {
            try {
                input.close();
            } catch (IOException ioe) {

            }
        }
    }

    public static Map<String, Integer> getImageWidthAndHeight(Uri uri, Context context) {
        InputStream input = null;
        try {
            input = context.getContentResolver().openInputStream(uri);
            // Get the dimensions of the bitmap
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(input, null, bmOptions);
            input.close();
            Map<String, Integer> dimensions = new HashMap<>();
            dimensions.put("width", bmOptions.outWidth);
            dimensions.put("height", bmOptions.outHeight);
            return dimensions;
        } catch (FileNotFoundException fne) {
            Log.e(TAG, "Failed to load image.", fne);
        } catch (Exception e) {
            Log.e(TAG, "Failed to load image.", e);
        } finally {
            try {
                input.close();
            } catch (IOException ioe) {
            }
        }
        return null;
    }

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String temporaryLocalImagePath = MediaStore.Images.Media
                .insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(temporaryLocalImagePath);
    }
}