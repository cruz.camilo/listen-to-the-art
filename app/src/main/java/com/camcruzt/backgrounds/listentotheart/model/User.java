package com.camcruzt.backgrounds.listentotheart.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

public class User implements Parcelable {

    private String id;
    private String email;
    private String contactEmail;
    private String name;
    private String username;
    private String country;
    private String website;
    private String aboutMe;
    private String photoUrl;
    private String numberPublications;
    private Boolean canPost;
    private HashMap<String, String> followedAccounts;

    public User(){
    }

    public User(String id, String email, String contactEmail, String name, String username,
                String country, String website, String aboutMe, String photoUrl,
                String numberPublications, Boolean canPost,
                HashMap<String, String> followedAccounts) {
        this.id = id;
        this.email = email;
        this.contactEmail = contactEmail;
        this.name = name;
        this.username = username;
        this.country = country;
        this.website = website;
        this.aboutMe = aboutMe;
        this.photoUrl = photoUrl;
        this.numberPublications = numberPublications;
        this.canPost = canPost;
        this.followedAccounts = followedAccounts;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getCountry() {
        return country;
    }

    public String getWebsite() {
        return website;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public String getNumberPublications() {
        return numberPublications;
    }

    public Boolean getCanPost() {
        return canPost;
    }

    public HashMap<String, String> getFollowedAccounts() {
        return followedAccounts;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", email='" + email + '\'' +
                ", contactEmail='" + contactEmail + '\'' +
                ", name='" + name + '\'' +
                ", username='" + username + '\'' +
                ", country='" + country + '\'' +
                ", website='" + website + '\'' +
                ", aboutMe='" + aboutMe + '\'' +
                ", photoUrl='" + photoUrl + '\'' +
                ", numberPublications='" + numberPublications + '\'' +
                ", followedAccounts=" + followedAccounts +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int i) {
        dest.writeString(id);
        dest.writeString(email);
        dest.writeString(name);
        dest.writeString(username);
        dest.writeString(country);
        dest.writeString(website);
        dest.writeString(aboutMe);
        dest.writeString(photoUrl);
        dest.writeString(numberPublications);
    }

    public User(Parcel parcel){
        id = parcel.readString();
        email = parcel.readString();
        name = parcel.readString();
        username = parcel.readString();
        country = parcel.readString();
        website = parcel.readString();
        aboutMe = parcel.readString();
        photoUrl = parcel.readString();
        numberPublications = parcel.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}