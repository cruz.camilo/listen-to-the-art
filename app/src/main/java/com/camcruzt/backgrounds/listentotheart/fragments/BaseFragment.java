package com.camcruzt.backgrounds.listentotheart.fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.SearchView;
import android.transition.Fade;
import android.view.View;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.camcruzt.backgrounds.listentotheart.R;
import com.camcruzt.backgrounds.listentotheart.activities.MainActivity;
import com.camcruzt.backgrounds.listentotheart.model.Artwork;
import com.camcruzt.backgrounds.listentotheart.model.User;
import com.camcruzt.backgrounds.listentotheart.utils.Utils;

import static com.camcruzt.backgrounds.listentotheart.activities.MainActivity.USER_ID_KEY;
import static com.camcruzt.backgrounds.listentotheart.fragments.ProfileFragment.PARCELABLE_KEY;


public abstract class BaseFragment extends Fragment {

    public BaseFragment() {
    }
    private android.support.v7.app.ActionBar supportActionBar;
    public SearchView searchView;
    private AHBottomNavigation bottomNavigation;
    public static final String USER_KEY = "user";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bottomNavigation = (AHBottomNavigation) getActivity().findViewById(R.id.bottom_navigation);
        searchView = ((MainActivity) getActivity()).findViewById(R.id.toolbar_search_view);
        supportActionBar = ((MainActivity) getActivity()).getSupportActionBar();
    }

    public void setSearchViewVisibility(Boolean visibility){
        if(visibility){
            searchView.setVisibility(View.VISIBLE);
        } else{
            searchView.setVisibility(View.INVISIBLE);
        }
    }

    public void showUserProfile(User user, String userId){
        Bundle bundle = new Bundle();
        bundle.putParcelable(USER_KEY, user);
        bundle.putString(USER_ID_KEY, userId);
        ProfileFragment profileFragment = new ProfileFragment();
        profileFragment.setArguments(bundle);

        ((MainActivity)getActivity()).replaceFragment(profileFragment, null);
    }



    public void switchToLargeArtworkFragment(Artwork artwork, View view) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(PARCELABLE_KEY, artwork);
        LargeArtworkFragment largeArtworkFragment = new LargeArtworkFragment();
        largeArtworkFragment.setArguments(bundle);


        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //largeArtworkFragment.setSharedElementReturnTransition(new Fade());
            //largeArtworkFragment.setExitTransition(TransitionInflater.from(getActivity()).inflateTransition(R.transition.fade));
            largeArtworkFragment.setSharedElementEnterTransition(new Fade());
            //largeArtworkFragment.setEnterTransition(TransitionInflater.from(getActivity()).inflateTransition(R.transition.fade));

            fragmentManager
                    .beginTransaction()
                    .addSharedElement(view.findViewById(R.id.artworkItemImageView), getString(R.string.image_transition))
                    .replace(R.id.content_frame, largeArtworkFragment)
                    .addToBackStack(null)
                    .commit();
        } else {
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.content_frame, largeArtworkFragment)
                    .addToBackStack(null)
                    .commit();
        }
    }

    public void returnToProfileScreen(int backNumber) {

            /*backNumber1 Return from share fragment screen.
            backNumber2 Return from edit artwork screen.
            backNumber3 Return from audio fragment*/

        for(int i=0; i<backNumber; i++){
            getActivity().getSupportFragmentManager().popBackStack();
        }
        Utils.hideKeyboardFrom(getContext(), getView());
        bottomNavigation.setCurrentItem(3);
    }
}