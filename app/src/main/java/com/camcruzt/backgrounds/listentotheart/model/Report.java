package com.camcruzt.backgrounds.listentotheart.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.ServerValue;

public class Report {

    private String userId;
    private String reportedArtwork;
    private String content;
    private Object timestamp;
    private Boolean reviewed;

    public Report(){}

    public Report(String userId, String reportedArtwork, String content) {
        this.userId = userId;
        this.reportedArtwork = reportedArtwork;
        this.content = content;
        this.timestamp = ServerValue.TIMESTAMP;
        this.reviewed = false;
    }

    public Report(String userId, String reportedArtwork, String content, Object timestamp, Boolean reviewed) {
        this.userId = userId;
        this.reportedArtwork = reportedArtwork;
        this.content = content;
        this.timestamp = timestamp;
        this.reviewed = reviewed;
    }

    public String getUserId() {
        return userId;
    }

    public String getReportedArtwork() {
        return reportedArtwork;
    }

    public String getContent() {
        return content;
    }

    public Object getTimestamp() {
        return timestamp;
    }

    public Boolean getReviewed() {
        return reviewed;
    }

    @Exclude
    public long getTimestampCreatedLong() {
        return (long) getTimestamp();
    }
}