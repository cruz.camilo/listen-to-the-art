package com.camcruzt.backgrounds.listentotheart.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;

public class Artwork implements Parcelable, Comparable<Artwork> {

    private String id;
    private String authorId;
    private String authorUsername;
    private String authorProfileImageUrl;
    private String title;
    private String year;
    private String height;
    private String width;
    private String medium;
    private String imageUrl;
    private String thumbnailUrl;
    private String description;
    private String additionalInfo;
    private Object timestamp;
    private HashMap<String, String> keywords;
    private String mainAudioUrl;
    private String mainAudioFilename;
    private String detailAudioUrl;
    private String detailAudioFilename;
    private String imageOrientation;

    public Artwork() {
    }

    public Artwork(String id, String authorId, String authorUsername, String authorProfileImageUrl,
                   String title, String year, String height, String width, String medium,
                   String imageUrl, String thumbnailUrl, String description, String additionalInfo, Object timestamp,
                   HashMap<String, String> keywords, String mainAudioUrl, String mainAudioFilename,
                   String detailAudioUrl, String detailAudioFilename, String imageOrientation) {
        this.id = id;
        this.authorId = authorId;
        this.authorUsername = authorUsername;
        this.authorProfileImageUrl = authorProfileImageUrl;
        this.title = title;
        this.year = year;
        this.height = height;
        this.width = width;
        this.medium = medium;
        this.imageUrl = imageUrl;
        this.thumbnailUrl = thumbnailUrl;
        this.description = description;
        this.additionalInfo = additionalInfo;
        this.timestamp = timestamp;
        this.keywords = keywords;
        this.mainAudioUrl = mainAudioUrl;
        this.mainAudioFilename = mainAudioFilename;
        this.detailAudioUrl = detailAudioUrl;
        this.detailAudioFilename = detailAudioFilename;
        this.imageOrientation = imageOrientation;
    }

    public Artwork(String id, String authorId, String authorUsername, String authorProfileImageUrl,
                   String title, String year, String height, String width, String medium,
                   String imageUrl, String thumbnailUrl, String description, String additionalInfo, String imageOrientation) {
        this.id = id;
        this.authorId = authorId;
        this.authorUsername = authorUsername;
        this.authorProfileImageUrl = authorProfileImageUrl;
        this.title = title;
        this.year = year;
        this.height = height;
        this.width = width;
        this.medium = medium;
        this.imageUrl = imageUrl;
        this.thumbnailUrl = thumbnailUrl;
        this.description = description;
        this.additionalInfo = additionalInfo;
        this.timestamp = ServerValue.TIMESTAMP;
        this.imageOrientation = imageOrientation;
    }

    public Artwork(String authorUsername, String title, String year, String imageUrl) {
        this.authorUsername = authorUsername;
        this.title = title;
        this.year = year;
        this.imageUrl = imageUrl;
    }

    public String getId() {
        return id;
    }

    public String getAuthorId() {
        return authorId;
    }

    public String getAuthorUsername() {
        return authorUsername;
    }

    public String getAuthorProfileImageUrl() {
        return authorProfileImageUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getYear() {
        return year;
    }

    public String getHeight() {
        return height;
    }

    public String getWidth() {
        return width;
    }

    public String getMedium() {
        return medium;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public String getDescription() {
        return description;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public Object getTimestamp() {
        return timestamp;
    }

    @Exclude
    public long getTimestampCreatedLong() {
        return (long) getTimestamp();
    }

    public HashMap<String, String> getKeywords() {
        return keywords;
    }

    public String getMainAudioUrl() {
        return mainAudioUrl;
    }

    public String getDetailAudioUrl() {
        return detailAudioUrl;
    }

    public String getMainAudioFilename() {
        return mainAudioFilename;
    }

    public String getDetailAudioFilename() {
        return detailAudioFilename;
    }

    public String getImageOrientation() {
        return imageOrientation;
    }

    @Override
    public String toString() {
        return "Artwork{" +
                "id='" + id + '\'' +
                ", authorId='" + authorId + '\'' +
                ", authorUsername='" + authorUsername + '\'' +
                ", authorProfileImageUrl='" + authorProfileImageUrl + '\'' +
                ", title='" + title + '\'' +
                ", year='" + year + '\'' +
                ", height='" + height + '\'' +
                ", width='" + width + '\'' +
                ", medium='" + medium + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", description='" + description + '\'' +
                ", additionalInfo='" + additionalInfo + '\'' +
                ", timestamp=" + timestamp +
                ", keywords=" + keywords +
                ", mainAudioUrl='" + mainAudioUrl + '\'' +
                ", mainAudioFilename='" + mainAudioFilename + '\'' +
                ", detailAudioUrl='" + detailAudioUrl + '\'' +
                ", detailAudioFilename='" + detailAudioFilename + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int i) {
        dest.writeString(id);
        dest.writeString(authorId);
        dest.writeString(authorUsername);
        dest.writeString(authorProfileImageUrl);
        dest.writeString(title);
        dest.writeString(year);
        dest.writeString(height);
        dest.writeString(width);
        dest.writeString(medium);
        dest.writeString(imageUrl);
        dest.writeString(thumbnailUrl);
        dest.writeString(description);
        dest.writeString(additionalInfo);
        dest.writeString(mainAudioUrl);
        dest.writeString(mainAudioFilename);
        dest.writeString(detailAudioUrl);
        dest.writeString(detailAudioFilename);
        dest.writeString(imageOrientation);
    }

    public Artwork(Parcel parcel) {
        id = parcel.readString();
        authorId = parcel.readString();
        authorUsername = parcel.readString();
        authorProfileImageUrl = parcel.readString();
        title = parcel.readString();
        year = parcel.readString();
        height = parcel.readString();
        width = parcel.readString();
        medium = parcel.readString();
        imageUrl = parcel.readString();
        thumbnailUrl = parcel.readString();
        description = parcel.readString();
        additionalInfo = parcel.readString();
        //timestamp = parcel.readLong();
        mainAudioUrl = parcel.readString();
        mainAudioFilename = parcel.readString();
        detailAudioUrl = parcel.readString();
        detailAudioFilename = parcel.readString();
        imageOrientation = parcel.readString();
    }


    public static final Parcelable.Creator<Artwork> CREATOR = new Parcelable.Creator<Artwork>() {

        public Artwork createFromParcel(Parcel in) {
            return new Artwork(in);
        }

        public Artwork[] newArray(int size) {
            return new Artwork[size];
        }
    };

    @Override
    public int compareTo(Artwork compareArtwork) {

        return Long.compare(compareArtwork.getTimestampCreatedLong(), (long)this.timestamp);
    }
}