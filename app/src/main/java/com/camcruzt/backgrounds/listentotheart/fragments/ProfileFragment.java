package com.camcruzt.backgrounds.listentotheart.fragments;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.AutoTransition;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.camcruzt.backgrounds.listentotheart.BuildConfig;
import com.camcruzt.backgrounds.listentotheart.R;
import com.camcruzt.backgrounds.listentotheart.activities.MainActivity;
import com.camcruzt.backgrounds.listentotheart.adapter.ArtworkRecylerAdapter;
import com.camcruzt.backgrounds.listentotheart.databinding.FragmentProfileBinding;
import com.camcruzt.backgrounds.listentotheart.model.Artwork;
import com.camcruzt.backgrounds.listentotheart.model.User;
import com.camcruzt.backgrounds.listentotheart.utils.Utils;
import com.camcruzt.backgrounds.listentotheart.viewmodel.ArtworkViewModel;
import com.camcruzt.backgrounds.listentotheart.viewmodel.UserViewModel;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.camcruzt.backgrounds.listentotheart.activities.MainActivity.USER_ID_KEY;

public class ProfileFragment extends BaseFragment implements ArtworkRecylerAdapter.ListItemClickListener {

    public static final String PARCELABLE_KEY = "artObject";
    public static final String DEF_PACKAGE = "com.camcruzt.backgrounds.listentotheart";
    private static final String SIGNED_USER_KEY = "signedUserKey";
    private static final String TAG = ProfileFragment.class.getName();
    private User mCurrentUser;
    private User mSignedUser;
    private FragmentProfileBinding mBinding;
    private String mSignedUserId;
    private String mSelectedUserUid;
    private ArtworkRecylerAdapter mAdapter;
    private List<Artwork> mArtworkList;
    private static final String SAVED_USER = "savedUser";
    private static final String SAVED_ARRAYLIST = "savedArray";
    private ArtworkViewModel mArtworkViewModel;
    private UserViewModel mUserViewModel;
    private int numberPublications;
    private Boolean followingThisAccount;
    private HashMap<String, String> followedAccounts;
    private boolean mIsLoading = false;
    private RecyclerView mRecyclerView;

    public ProfileFragment() {
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(USER_ID_KEY, mSignedUserId);
        outState.putParcelable(SAVED_USER, mCurrentUser);
        if (mSignedUser != null) {
            outState.putParcelable(SIGNED_USER_KEY, mSignedUser);
        }
        outState.putParcelableArrayList(SAVED_ARRAYLIST, (ArrayList<? extends Parcelable>) mArtworkList);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        mSignedUserId = sharedPref.getString(getString(R.string.user_id_key_sharedpreferences), "");
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_profile, container, false);

        View rootView = mBinding.getRoot();
        if (searchView != null) {
            setSearchViewVisibility(false);
        }

        mRecyclerView = mBinding.artworksRecyclerView;
        if(mAdapter==null){
            mAdapter = new ArtworkRecylerAdapter(getContext(), new ArrayList<Artwork>(), this);
        }
        mRecyclerView.setAdapter(mAdapter);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        mRecyclerView.setLayoutManager(gridLayoutManager);

        mArtworkViewModel = ViewModelProviders.of(this).get(ArtworkViewModel.class);
        mUserViewModel = ViewModelProviders.of(this).get(UserViewModel.class);

        Bundle bundle = getArguments();
        mArtworkList = new ArrayList<>();
        setTextViewsVisibility(View.INVISIBLE);

        if (savedInstanceState != null && !savedInstanceState.isEmpty()) {
            mCurrentUser = savedInstanceState.getParcelable(SAVED_USER);
            mSignedUser = savedInstanceState.getParcelable(SIGNED_USER_KEY);
            mSignedUserId = savedInstanceState.getString(USER_ID_KEY);
            if(mCurrentUser != null){
                updateUI(mCurrentUser);
                setTextViewsVisibility(View.VISIBLE);
            }
            mArtworkList = savedInstanceState.getParcelableArrayList(SAVED_ARRAYLIST);
            if (mArtworkList != null && mArtworkList.size() > 0 && mAdapter.getItemCount() == 0) {
                Collections.reverse(mArtworkList);
                mAdapter.setData(mArtworkList);
            }
        } else {
            if (bundle != null && bundle.containsKey(USER_KEY)) {
                mCurrentUser = (User) bundle.getParcelable(USER_KEY);
                mSelectedUserUid = bundle.getString(USER_ID_KEY);
                if(mCurrentUser!=null){
                    updateViews();
                } else {
                    getUserById(mSelectedUserUid);
                }

            } else {
                if (!mSignedUserId.equals("")) {
                    getSignedUser();
                }
            }

            String artworkAuthorId;
            if (mSelectedUserUid == null) {
                artworkAuthorId = mSignedUserId;
            } else {
                artworkAuthorId = mSelectedUserUid;
            }
            getArtworksFromDb(artworkAuthorId);
        }

        mBinding.editFollowProfileButton.setOnClickListener(v -> {
            if (mSignedUserId.equals(mCurrentUser.getId())) {
                switchToEditProfileFragment();
            } else if (!followingThisAccount) {
                followProfile(mCurrentUser.getId(), false);
            } else {
                followProfile(mCurrentUser.getId(), true);
            }
        });

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int totalItemCount = gridLayoutManager.getItemCount();
                int lastVisible = gridLayoutManager.findLastCompletelyVisibleItemPosition();

                boolean endHasBeenReached = lastVisible +1 >= totalItemCount;
                if (!mIsLoading && totalItemCount > 0 && endHasBeenReached
                        && gridLayoutManager.findFirstCompletelyVisibleItemPosition()!=0) {
                    Log.d("Home", "end reached!");
                    Toast.makeText(getContext(), "End!", Toast.LENGTH_SHORT).show();
                    //getArtworks(mHomeAdapter.getLastItemTimestamp());
                    mIsLoading = true;
                }
            }
        });

        return rootView;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void updateViews() {
        updateUI(mCurrentUser);
        setTextViewsVisibility(View.VISIBLE);
        mSelectedUserUid = mCurrentUser.getId();
        getSignedUser();
    }

    private void getArtworksFromDb(String artworkAuthorId) {
        //Query DB and return current User publications.
        Query getUserArtworks = mArtworkViewModel.dataRef.orderByChild("authorId").equalTo(artworkAuthorId);
        LiveData<List<Artwork>> artworkLiveData = mArtworkViewModel.getArtworkListLiveData(getUserArtworks);
        artworkLiveData.observe(this, new Observer<List<Artwork>>() {
            @Override
            public void onChanged(@Nullable List<Artwork> artworks) {
                mAdapter.clear();
                mArtworkList.clear();
                if (!artworks.isEmpty()){
                    mArtworkList.addAll(artworks);
                    Collections.reverse(mArtworkList);
                }
                mAdapter.setData(mArtworkList);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void getSignedUser() {
        //Query DB and return signed User.
        LiveData<User> userLiveData = mUserViewModel.getUserById(mSignedUserId);
        userLiveData.observe(this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                if (user != null) {
                    mSignedUser = user;
                    if (mCurrentUser == null || mCurrentUser.getId().equals(mSignedUser.getId())) {
                        numberPublications = Integer.parseInt(mSignedUser.getNumberPublications());
                        updateUI(mSignedUser);
                        mCurrentUser = mSignedUser;
                        setTextViewsVisibility(View.VISIBLE);
                    } else if (!mCurrentUser.getId().equals(mSignedUser.getId())) {
                        followedAccounts = mSignedUser.getFollowedAccounts();
                        updateFollowingBtn();
                    }
                }
            }
        });
    }

    private void updateFollowingBtn() {
        if (followedAccounts != null && followedAccounts.containsKey(mCurrentUser.getId())) {
            followingThisAccount = true;
            mBinding.editFollowProfileButton.setText(getString(R.string.unfollow));
        } else {
            followingThisAccount = false;
            mBinding.editFollowProfileButton.setText(getString(R.string.follow));
        }
    }

    private void followProfile(String followedUserId, Boolean alreadyFollowing) {
        mUserViewModel.followAccount(mSignedUserId, followedUserId, alreadyFollowing);
        updateFollowingBtn();
    }

    private void setTextViewsVisibility(int visibility) {
        mBinding.profileNameTextView.setVisibility(visibility);
        mBinding.publicationNumberTextView.setVisibility(visibility);
        mBinding.countryTextView.setVisibility(visibility);
        mBinding.aboutMeTextView.setVisibility(visibility);
        mBinding.websiteTextView.setVisibility(visibility);
        mBinding.publicationsLabelTextView.setVisibility(visibility);
        mBinding.editFollowProfileButton.setVisibility(visibility);
    }

    private void updateUI(@NonNull User user) {
        if (user.getUsername() != null) {
            ((MainActivity) getActivity()).updateToolbarTitle(user.getUsername());
        } else {
            ((MainActivity) getActivity()).updateToolbarTitle(user.getName());
        }

        mBinding.profileNameTextView.setText(user.getName());

        if(mSignedUserId.equals(user.getId())){
            mBinding.editFollowProfileButton.setText(getString(R.string.update_profile));
        }

        mBinding.publicationNumberTextView.setText(user.getNumberPublications());

        String lowerCaseCountry = user.getCountry().toLowerCase();

        // Taken from https://stackoverflow.com/questions/6583843/
        int idFlagDrawable = getResources().getIdentifier("flag_" + lowerCaseCountry,
                "drawable", DEF_PACKAGE);
        mBinding.countryFlag.setImageResource(idFlagDrawable);

        if (user.getCountry().isEmpty() && mSignedUserId.equals(user.getId())) {
            mBinding.countryTextView.setHint(getResources().getString(R.string.country));
        } else {
            mBinding.countryTextView.setText(user.getCountry());
        }

        //TODO Verify layout so views can be gone
        if (user.getContactEmail().isEmpty() && !mSignedUserId.equals(user.getId())) {
            mBinding.contactEmailTextView.setVisibility(View.INVISIBLE);
        } else if (mSignedUserId.equals(user.getId()) && user.getContactEmail().isEmpty()) {
            mBinding.contactEmailTextView.setHint(getResources().getString(R.string.email_hint));
        } else {
            mBinding.contactEmailTextView.setText(user.getContactEmail());
        }

        if (user.getWebsite().isEmpty() && !mSignedUserId.equals(user.getId())) {
            mBinding.websiteTextView.setVisibility(View.INVISIBLE);
        } else if (user.getWebsite().isEmpty() && mSignedUserId.equals(user.getId())) {
            mBinding.websiteTextView.setHint(getResources().getString(R.string.website_hint));
        } else {
            mBinding.websiteTextView.setText(user.getWebsite());
        }

        if (user.getAboutMe().isEmpty() && !mSignedUserId.equals(user.getId())) {
            mBinding.aboutMeTextView.setVisibility(View.GONE);
        } else if (user.getAboutMe().isEmpty() && mSignedUserId.equals(user.getId())) {
            mBinding.aboutMeTextView.setHint(getResources().getString(R.string.about_me_hint));
        } else {
            mBinding.aboutMeTextView.setText(user.getAboutMe());
        }

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .circleCrop()
                .placeholder(new ColorDrawable(Color.WHITE))
                .error(R.drawable.free_user_icon_21);

        Glide.with(this)
                .load(user.getPhotoUrl())
                .apply(options).
                into(mBinding.profileImageView);
    }

    @Override
    public void onArtworkClick(int clickedItemIndex, View view) {
        Artwork selectedArtwork = mArtworkList.get(clickedItemIndex);
        switchToLargeArtworkFragment(selectedArtwork, view);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addMockArt:
                addMockArt();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(BuildConfig.BUILD_TYPE.equalsIgnoreCase("debug")) {
            inflater.inflate(R.menu.profile_menu, menu);
        }
    }

    //This method is used for testing purposes only. It will not be included on the release version
    private void addMockArt() {
        DatabaseReference dataRef = mArtworkViewModel.dataRef;
        String userId = mCurrentUser.getId();
        String artworkId = dataRef.push().getKey();
        Artwork mockArtwork = Utils.createMockArtwork(artworkId, userId);
        dataRef.child(artworkId).setValue(mockArtwork);
        mUserViewModel.updateCounter(userId, numberPublications + 1);
        Toast.makeText(getContext(), "Art Example Published!", Toast.LENGTH_SHORT).show();
    }

    public void switchToEditProfileFragment() {
        EditProfileFragment editProfileFragment = new EditProfileFragment();

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.setSharedElementReturnTransition(new AutoTransition());
            this.setExitTransition(TransitionInflater.from(getActivity()).inflateTransition(R.transition.slide_in));
            editProfileFragment.setSharedElementEnterTransition(new AutoTransition());

            fragmentManager
                    .beginTransaction()
                    .addSharedElement(mBinding.profileImageView, getString(R.string.profile_transition))
                    .replace(R.id.content_frame, editProfileFragment)
                    .addToBackStack(null)
                    .commit();
        } else {
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.content_frame, editProfileFragment)
                    .addToBackStack(null)
                    .commit();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void getUserById(String authorId) {
        LiveData<User> userLiveData = mUserViewModel.getUserById(authorId);
        userLiveData.observe(this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                mCurrentUser = user;
                if(mCurrentUser!=null){
                    updateViews();
                }
            }
        });
    }
}