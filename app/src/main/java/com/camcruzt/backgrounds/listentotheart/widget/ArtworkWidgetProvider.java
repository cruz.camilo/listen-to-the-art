package com.camcruzt.backgrounds.listentotheart.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.widget.RemoteViews;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.AppWidgetTarget;
import com.bumptech.glide.request.transition.Transition;
import com.camcruzt.backgrounds.listentotheart.R;
import com.camcruzt.backgrounds.listentotheart.activities.MainActivity;
import com.camcruzt.backgrounds.listentotheart.model.Artwork;

import static com.camcruzt.backgrounds.listentotheart.widget.ArtworkService.ARTWORK_KEY;
import static com.camcruzt.backgrounds.listentotheart.widget.ArtworkService.UPDATE_WIDGET;

public class ArtworkWidgetProvider extends AppWidgetProvider {

    static Artwork mArtwork;

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, Artwork artwork,
                                int appWidgetId) {

        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_layout);

        //Intent to launch main activity when clicked
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(ARTWORK_KEY, mArtwork);
        /*intent.setData(Uri.parse(String.valueOf(appWidgetId)));
        intent.setAction("SOME_PREFIX_" + appWidgetId);*/
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        views.setOnClickPendingIntent(R.id.widgetImageIv, pendingIntent);

        if(mArtwork == null){
            CharSequence widgetText = context.getString(R.string.app_name);
            views.setImageViewResource(R.id.widgetImageIv, R.drawable.logo);
            views.setTextViewText(R.id.widgetTitleTv, widgetText);
        }

        if(mArtwork!=null){
            AppWidgetTarget widgetTarget = new AppWidgetTarget(context, R.id.widgetImageIv, views, appWidgetId) {
                @Override
                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                    super.onResourceReady(resource, transition);
                }
            };

            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .override(400, 400)
                    .error(R.drawable.logo);

            Glide.with(context.getApplicationContext())
                    .asBitmap()
                    .load(mArtwork.getImageUrl())
                    .apply(options)
                    .into(widgetTarget);

            views.setTextViewText(R.id.widgetTitleTv, mArtwork.getTitle());
            views.setTextViewText(R.id.widgetAuthorTv, (context.getString(R.string.author_comma, mArtwork.getAuthorUsername())));
            views.setTextViewText(R.id.widgetYearTv, mArtwork.getYear());
        }

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, mArtwork, appWidgetId);
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    public static void updateWidgetArtwork(Context context, AppWidgetManager appWidgetManager,
                                           Artwork artwork, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, artwork, appWidgetId);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        AppWidgetManager mgr = AppWidgetManager.getInstance(context);
        ComponentName cn = new ComponentName(context, ArtworkWidgetProvider.class);
        int[] appWidgetIds = mgr.getAppWidgetIds(new ComponentName(context, ArtworkWidgetProvider.class));
        final String action = intent.getAction();

        if (action.equals(UPDATE_WIDGET)) {
            // refresh all widgets
            mArtwork = intent.getParcelableExtra(ARTWORK_KEY);
            mgr.notifyAppWidgetViewDataChanged(mgr.getAppWidgetIds(cn), R.id.widgetImageIv);
            mgr.notifyAppWidgetViewDataChanged(mgr.getAppWidgetIds(cn), R.id.widgetTitleTv);
            mgr.notifyAppWidgetViewDataChanged(mgr.getAppWidgetIds(cn), R.id.widgetAuthorTv);
            mgr.notifyAppWidgetViewDataChanged(mgr.getAppWidgetIds(cn), R.id.widgetYearTv);

            ArtworkWidgetProvider.updateWidgetArtwork(context, mgr, mArtwork, appWidgetIds);
        }
        super.onReceive(context, intent);
    }
}

