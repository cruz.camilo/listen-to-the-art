package com.camcruzt.backgrounds.listentotheart.fragments;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.camcruzt.backgrounds.listentotheart.R;
import com.camcruzt.backgrounds.listentotheart.activities.MainActivity;
import com.camcruzt.backgrounds.listentotheart.databinding.FragmentProfileEditBinding;
import com.camcruzt.backgrounds.listentotheart.model.User;
import com.camcruzt.backgrounds.listentotheart.utils.Utils;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class EditProfileFragment extends Fragment {

    private static final String TAG = EditProfileFragment.class.getName();
    private static final int RC_PHOTO_PICKER = 2;
    private static final String NAME_KEY = "name";
    private static final String COUNTRY_KEY = "country";
    private static final String WEBSITE_KEY = "website";
    private static final String ABOUT_ME_KEY = "aboutMe";
    private static final String CONTACT_EMAIL_KEY = "contactEmail";
    private static final String USERNAME_KEY = "username";
    private DatabaseReference mCurrentUserDatabaseReference;
    private DatabaseReference mUsersDatabaseReference;
    private StorageReference mProfilePhotos;
    private StorageReference mPhotoStorageRef;
    private User mUser;
    private ValueEventListener mValueEventListener;
    private FragmentProfileEditBinding mBinding;
    private String mSignedUserId;
    private Boolean mNewPhotoSelected = false;
    private Uri mSelectedImageUri;
    private String mPicturePath;
    private Boolean isDataValidated = false;
    public boolean mProfileHasChanged = false;
    private View.OnTouchListener mTouchListener;
    private final int PICK_FROM_GALLERY = 1;

    public EditProfileFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_profile_edit, container, false);
        View rootView = mBinding.getRoot();
        //TODO verify refactor
        ((MainActivity) getActivity()).updateToolbarTitle(getString(R.string.edit_profile_label));
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        mSignedUserId = sharedPref.getString(getString(R.string.user_id_key_sharedpreferences), "");

        mUsersDatabaseReference = ((MainActivity) getActivity()).mUsersDatabaseReference;
        mCurrentUserDatabaseReference = ((MainActivity) getActivity())
                .mUsersDatabaseReference.child(mSignedUserId);

        mProfilePhotos = ((MainActivity) getActivity()).mProfilePhotos;

        mValueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mUser = dataSnapshot.getValue(User.class);
                if (!mUser.getPhotoUrl().isEmpty() && mUser.getPhotoUrl() != null) {
                    mPhotoStorageRef = ((MainActivity) getActivity()).mFirebaseStorage.getReferenceFromUrl(mUser.getPhotoUrl());
                }
                updateUI();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        };

        //onClickListener on profile image to open photo picker and select a photo
        mBinding.userProfileImageView.setOnClickListener(v -> {
            if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                if (getContext().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    /*if (shouldShowRequestPermissionRationale(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    }*/

                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            PICK_FROM_GALLERY);
                } else {
                    openGallery();
                }
            } else {
                openGallery();
            }

        });

        mTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mProfileHasChanged = true;
                return false;
            }
        };
        rootView.setOnTouchListener(mTouchListener);
        setOnTouchListener();

        return rootView;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PICK_FROM_GALLERY) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openGallery();
            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.profile_permission), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void setOnTouchListener() {
        mBinding.nameEditText.setOnTouchListener(mTouchListener);
        mBinding.usernameEditText.setOnTouchListener(mTouchListener);
        mBinding.contactEmailEditText.setOnTouchListener(mTouchListener);
        mBinding.countryPicker.setOnTouchListener(mTouchListener);
        mBinding.websiteProfileEditText.setOnTouchListener(mTouchListener);
        mBinding.profileAboutMeEditText.setOnTouchListener(mTouchListener);
    }


    public void openGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, RC_PHOTO_PICKER);
    }

    private Boolean updateUser() {
        String name = mBinding.nameEditText.getText().toString();
        if(name.isEmpty()){
            Toast.makeText(getContext(), getString(R.string.name_validation), Toast.LENGTH_LONG).show();
            return isDataValidated;
        }
        String username = mBinding.usernameEditText.getText().toString().toLowerCase();
        if(username.isEmpty()){
            Toast.makeText(getContext(), getString(R.string.username_validation), Toast.LENGTH_LONG).show();
            return isDataValidated;
        }
        String country = mBinding.countryPicker.getSelectedCountryName();
        String website = mBinding.websiteProfileEditText.getText().toString();
        String aboutMe = mBinding.profileAboutMeEditText.getText().toString();
        String contactEmail = mBinding.contactEmailEditText.getText().toString();
        Map<String, Object> userUpdates = new HashMap<>();
        userUpdates.put(NAME_KEY, name);
        userUpdates.put(USERNAME_KEY, username);
        userUpdates.put(COUNTRY_KEY, country);
        userUpdates.put(WEBSITE_KEY, website);
        userUpdates.put(ABOUT_ME_KEY, aboutMe);
        userUpdates.put(CONTACT_EMAIL_KEY, contactEmail);
        mCurrentUserDatabaseReference.updateChildren(userUpdates);
        return isDataValidated = true;
    }

    private class SaveUserAsyncTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... avoid) {
            Boolean isDataValidated = updateUser();
            return isDataValidated;
        }

        @Override
        protected void onPostExecute(Boolean isDataValidated) {
            super.onPostExecute(isDataValidated);
            if(isDataValidated){
                if(mSelectedImageUri!=null){
                    int profileImageW = 250;
                    int profileImageH = 250;
                    mSelectedImageUri = Utils.getImageUri(getContext(),
                            Utils.getBitmapFromUri(mSelectedImageUri, profileImageW, profileImageH, getContext()));
                    updatePhotoStorage(mSelectedImageUri);
                }
                Utils.hideKeyboardFrom(getContext(), getView());
                Toast.makeText(getActivity(), getString(R.string.save_message), Toast.LENGTH_LONG).show();
                ((MainActivity)getActivity()).replaceFragment(new ProfileFragment(), null);
            }
        }
    }

    private void updateUI() {
        mBinding.nameEditText.setText(mUser.getName());
        mBinding.websiteProfileEditText.setText(mUser.getWebsite());
        mBinding.profileAboutMeEditText.setText(mUser.getAboutMe());
        mBinding.usernameEditText.setText(mUser.getUsername());
        mBinding.contactEmailEditText.setText(mUser.getContactEmail());

        RequestOptions mOptions = new RequestOptions()
                .centerCrop()
                .circleCrop()
                .placeholder(new ColorDrawable(Color.WHITE))
                .error(R.drawable.free_user_icon_21);

        //mNewPhotoSelected = false, shows image from DB,
        //mNewPhotoSelected = true shows updated photo
        if (!mNewPhotoSelected) {
            Glide.with(this)
                    .load(mUser.getPhotoUrl())
                    .apply(mOptions)
                    .into(mBinding.userProfileImageView);
        } else {
            mProfileHasChanged = true;
            Glide.with(this)
                    .load(mPicturePath)
                    .apply(mOptions)
                    .into(mBinding.userProfileImageView);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mValueEventListener!=null){
            addChildEventListener();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mValueEventListener!=null) {
            mCurrentUserDatabaseReference.removeEventListener(mValueEventListener);
        }
    }

    private void addChildEventListener() {
        mUsersDatabaseReference.child(mSignedUserId).addValueEventListener(mValueEventListener);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Activity result after selecting image
        if (requestCode == RC_PHOTO_PICKER && resultCode == RESULT_OK) {
            mNewPhotoSelected = true;
            mSelectedImageUri = data.getData();
            if (mSelectedImageUri !=null){
                mPicturePath = Utils.getFilePath(mSelectedImageUri, getContext());
            }
        }
    }

    private void updatePhotoStorage(Uri selectedImageUri) {
        final StorageReference photoRef =
                mProfilePhotos.child(mSignedUserId).child(selectedImageUri.getLastPathSegment());

        //Taken from https://github.com/udacity/and-nd-firebase/issues/41
        photoRef.putFile(selectedImageUri)
                .continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }
                        return photoRef.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    //TODO copy method to artwork
                    //using saved URL on database deletes previous image from Firebase Storage,
                    // so only 1 profile image will be saved.
                    //Taken from: https://stackoverflow.com/questions/42930619/
                    if (mPhotoStorageRef != null) {
                        mPhotoStorageRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d(TAG, "onSuccess: deleted file");
                            }
                        });
                    }
                    Uri downloadUri = task.getResult();
                    Map<String, Object> userUpdates = new HashMap<>();
                    userUpdates.put("photoUrl", downloadUri.toString());
                    mCurrentUserDatabaseReference.updateChildren(userUpdates);

                    String temporaryImagePath = Utils.getFilePath(mSelectedImageUri, getContext());
                    ContentResolver contentResolver = getActivity().getContentResolver();
                    contentResolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            MediaStore.Images.ImageColumns.DATA + "=?", new String[]{temporaryImagePath});
                } else {
                    Toast.makeText(getActivity(), "upload failed: " + task.getException()
                            .getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.edit_menu, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item2 = menu.findItem(R.id.edit_audio_menu);
        item2.setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sign_out_menu:
                AuthUI.getInstance().signOut(getContext());
                return true;
            case R.id.updateBtn:
                SaveUserAsyncTask asyncTask = new SaveUserAsyncTask();
                asyncTask.execute();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}