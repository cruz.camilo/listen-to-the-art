package com.camcruzt.backgrounds.listentotheart.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.camcruzt.backgrounds.listentotheart.R;

public class ContactFragment extends BaseFragment {

    public ContactFragment(){
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact, container, false);

        return view;
    }
}
