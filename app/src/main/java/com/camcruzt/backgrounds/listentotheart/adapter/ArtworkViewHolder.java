package com.camcruzt.backgrounds.listentotheart.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.camcruzt.backgrounds.listentotheart.R;

public class ArtworkViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public ImageView artworkImageView;
    public ListItemClickListener mOnClickListener;

    public ArtworkViewHolder(@NonNull View itemView) {
        super(itemView);
        artworkImageView = (ImageView)itemView.findViewById(R.id.artworkItemImageView);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int clickedPosition = getAdapterPosition();
        View view = v;
        mOnClickListener.onArtworkClick(clickedPosition, v);
    }

    public interface ListItemClickListener {
        void onArtworkClick(int clickedItemIndex, View view);
    }
}
