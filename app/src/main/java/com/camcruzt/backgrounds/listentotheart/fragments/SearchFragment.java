package com.camcruzt.backgrounds.listentotheart.fragments;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.camcruzt.backgrounds.listentotheart.R;
import com.camcruzt.backgrounds.listentotheart.activities.MainActivity;
import com.camcruzt.backgrounds.listentotheart.adapter.ArtworkRecylerAdapter;
import com.camcruzt.backgrounds.listentotheart.adapter.UserRecyclerAdapter;
import com.camcruzt.backgrounds.listentotheart.databinding.FragmentSearchBinding;
import com.camcruzt.backgrounds.listentotheart.model.Artwork;
import com.camcruzt.backgrounds.listentotheart.model.User;
import com.camcruzt.backgrounds.listentotheart.utils.Utils;
import com.camcruzt.backgrounds.listentotheart.viewmodel.ArtworkViewModel;
import com.camcruzt.backgrounds.listentotheart.viewmodel.UserViewModel;
import com.google.android.gms.ads.AdRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class SearchFragment extends BaseFragment implements ArtworkRecylerAdapter.ListItemClickListener,
        UserRecyclerAdapter.ListItemClickListener {

    private static final String KEYWORDS_KEY = "keywords/";
    private static final String USERNAME_KEY = "username";
    private static final String TITLE_KEY = "title";
    private FragmentSearchBinding mBinding;
    private Parcelable mLayoutSavedState;

    public SearchFragment() {
    }

    private String orderCriteria;
    private Query searchQuery;
    private String mQuery = "";
    private ArtworkRecylerAdapter mArtworkAdapter;
    private RecyclerView mRecyclerView;
    private List<Artwork> mArtworkList = new ArrayList<>();;
    private UserRecyclerAdapter mUserAdapter;
    private ArrayList<User> mUserList = new ArrayList<>();
    private DatabaseReference artworkDataRef;
    private DatabaseReference userDataRef;
    private RecyclerView.Adapter mCurrentAdapter;
    private GridLayoutManager mGridLayoutManager;
    private SharedPreferences sharedPref;
    private int spinnerPosition;
    private int mArtworksPerPage = 15;
    private boolean mIsLoading = false;
    private Spinner mSpinner;
    private UserViewModel mUserViewModel;
    private ArtworkViewModel mArtworkViewModel;
    private LiveData<List<User>> mUserListLiveData;
    private LiveData<List<Artwork>> mArtworkListLiveData;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_search, container, false);
        View rootView = mBinding.getRoot();

        ((MainActivity) getActivity()).updateToolbarTitle(getContext().getString(R.string.search_title));
        if (searchView != null) {
            setSearchViewVisibility(true);
            searchView.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
            searchView.setQueryHint(getString(R.string.search));
        }

        mRecyclerView = mBinding.resultsRecyclerView;
        if(mArtworkAdapter==null){
            mArtworkAdapter = new ArtworkRecylerAdapter(getContext(), new ArrayList<>(), this);
            mRecyclerView.setAdapter(mArtworkAdapter);
        }
        if(mUserAdapter==null){
            mUserAdapter = new UserRecyclerAdapter(getContext(), mUserList, this);
        }

        mGridLayoutManager = new GridLayoutManager(getActivity(), 3);
        mRecyclerView.setLayoutManager(mGridLayoutManager);

        mArtworkViewModel =
                ViewModelProviders.of(this).get(ArtworkViewModel.class);
        mUserViewModel =
                ViewModelProviders.of(this).get(UserViewModel.class);
        artworkDataRef = mArtworkViewModel.dataRef;
        userDataRef = mUserViewModel.dataRef;

        searchQuery = artworkDataRef
                .orderByChild("timestamp")
                .limitToLast(mArtworksPerPage);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int totalItemCount = mGridLayoutManager.getItemCount();
                int lastVisible = mGridLayoutManager.findLastCompletelyVisibleItemPosition();

                boolean endHasBeenReached = lastVisible +3 >= totalItemCount;
                if (!mIsLoading && totalItemCount > 0 && endHasBeenReached
                        && mGridLayoutManager.findFirstCompletelyVisibleItemPosition()!=0) {

                    if(orderCriteria.equals(KEYWORDS_KEY) || orderCriteria.equals(TITLE_KEY)){
                        Log.d("Home", "end reached!");

                        searchQuery = artworkDataRef
                                .orderByChild("timestamp")
                                .endAt(mArtworkAdapter.getLastItemTimestamp()+1)
                                .limitToLast(mArtworksPerPage);

                        getArtworks(searchQuery);
                        mIsLoading = true;
                    }
                }
            }
        });

        if(mArtworkList.size() == 0){
            getArtworks(searchQuery);
        }

        if (searchView != null) {
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String query) {
                    mCurrentAdapter = mRecyclerView.getAdapter();
                    mQuery = query.replaceAll("[.#\\[\\]\\$]", "");
                    mQuery = mQuery.trim();
                    mQuery = Utils.convertToTitleCaseIteratingChars(mQuery);

                    if (mCurrentAdapter == mArtworkAdapter) {
                        mArtworkAdapter.clear();
                        setArtworkQuery();
                    } else if (mCurrentAdapter == mUserAdapter) {
                        mUserAdapter.clear();
                        setUsernameQuery();
                    }
                    return true;
                }
            });
        }

        if(isAdded()){
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice("E04EC52520C747F61516FD226CC32D0F")
                    .build();
            mBinding.searchFragmentadView.loadAd(adRequest);
        }
        return rootView;
    }

    private void getArtworks(Query searchQuery) {
        int adapterItemCount = mArtworkAdapter.getItemCount();
        mArtworkListLiveData = mArtworkViewModel.getArtworkListLiveData(searchQuery);
        mArtworkListLiveData.observe(this, new Observer<List<Artwork>>() {
            @Override
            public void onChanged(@Nullable List<Artwork> artworks) {
                mArtworkList = artworks;
                Collections.reverse(mArtworkList);
                for (Artwork artwork : mArtworkList){
                    if (!mArtworkAdapter.hasBeenAdded(artwork.getId())) {
                        mArtworkAdapter.addArt(artwork);
                    }
                }

                if(mArtworkAdapter.getItemCount()>2 && mArtworkAdapter.get(0).getTimestampCreatedLong()<mArtworkAdapter.get(1).getTimestampCreatedLong()){
                    mArtworkAdapter.swapItems(0);
                }

                if (adapterItemCount != mArtworkAdapter.getItemCount()  && mIsLoading){
                    mIsLoading = false;
                }

                if(mLayoutSavedState !=null){
                    mRecyclerView.getLayoutManager().onRestoreInstanceState(mLayoutSavedState);
                }
                mArtworkAdapter.notifyDataSetChanged();
            }
        });
    }

    private void setArtworkQuery() {
        if (mQuery.length() == 0 && orderCriteria.equals("keywords/")
                || orderCriteria.equals("title") && mQuery.length() == 0) {
            searchQuery = artworkDataRef
                    .orderByChild("timestamp")
                    .limitToLast(mArtworksPerPage);
        } else if (orderCriteria.equals("keywords/") && mQuery.length() > 0) {
            searchQuery = artworkDataRef.orderByChild(orderCriteria + mQuery).equalTo("true");
        } else if (orderCriteria.equals("title")) {
            searchQuery = artworkDataRef.orderByChild(orderCriteria).equalTo(mQuery);
        }
        getArtworks(searchQuery);
    }

    private void setUsernameQuery() {
        if (orderCriteria.equals("username")) {
            if (mQuery==null || mQuery.length() == 0) {
                searchQuery = userDataRef.orderByChild(orderCriteria);
            } else {
                //Taken from https://stackoverflow.com/questions/50812056/
                searchQuery = userDataRef
                        .orderByChild(orderCriteria)
                        .startAt(mQuery.toLowerCase())
                        .endAt(mQuery.toLowerCase() + "\uf8ff");
            }
        }

        mUserListLiveData = mUserViewModel.getUserListLiveData(searchQuery);
        mUserListLiveData.observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(@Nullable List<User> users) {
                ArrayList<User> displayedUsers = new ArrayList<>();
                for (User user: users){
                    if (Integer.valueOf(user.getNumberPublications()) > 0){
                        displayedUsers.add(user);
                    }
                }
                mUserAdapter.setData(displayedUsers);
            }
        });
    }


    @Override
    public void onPause() {
        super.onPause();
        Utils.hideKeyboardFrom(getContext(), getView());
        mLayoutSavedState = mGridLayoutManager.onSaveInstanceState();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);

        MenuItem item = menu.findItem(R.id.spinner_menu);
        mSpinner = (Spinner) item.getActionView();
        setupSpinner(mSpinner);

        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mCurrentAdapter = mRecyclerView.getAdapter();

                SharedPreferences.Editor editor = sharedPref.edit();
                if (position == 0) {
                    orderCriteria = KEYWORDS_KEY;
                    editor.putInt(getString(R.string.order_criteria_key_sharedpreferences),
                            0);
                    editor.apply();
                    if (mCurrentAdapter != mArtworkAdapter) {
                        mRecyclerView.setAdapter(mArtworkAdapter);
                        mRecyclerView.setLayoutManager(mGridLayoutManager);
                        if(searchView!=null){
                            searchView.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
                        }
                        setArtworkQuery();
                    }
                }
                if (position == 1) {
                    orderCriteria = USERNAME_KEY;
                    editor.putInt(getString(R.string.order_criteria_key_sharedpreferences),
                            1);
                    editor.apply();
                    if (mCurrentAdapter != mUserAdapter) {
                        mUserAdapter.clear();
                        mRecyclerView.setAdapter(mUserAdapter);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                        mRecyclerView.setLayoutManager(linearLayoutManager);
                        if(searchView!=null){
                            searchView.setInputType(InputType.TYPE_CLASS_TEXT);
                        }
                        setUsernameQuery();
                    }
                } else if (position == 2) {
                    orderCriteria = "title";
                    editor.putInt(getString(R.string.order_criteria_key_sharedpreferences),
                            2);
                    editor.apply();
                    if (mCurrentAdapter != mArtworkAdapter) {
                        mRecyclerView.setAdapter(mArtworkAdapter);
                        mRecyclerView.setLayoutManager(mGridLayoutManager);
                        if(searchView!=null){
                            searchView.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
                        }
                        setArtworkQuery();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void setupSpinner(Spinner spinner) {
        if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            spinner.setPopupBackgroundResource(R.drawable.spinner_background);
        }

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.search_criteria, R.layout.custom_spinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        spinnerPosition = sharedPref.getInt(getString(R.string.order_criteria_key_sharedpreferences), 0);
        spinner.setSelection(spinnerPosition);
    }

    @Override
    public void onArtworkClick(int clickedItemIndex, View view) {
        Artwork selectedArtwork = mArtworkAdapter.get(clickedItemIndex);
        switchToLargeArtworkFragment(selectedArtwork, view);
    }

    @Override
    public void onUserClick(int clickedItemIndex) {
        User user = mUserList.get(clickedItemIndex);
        showUserProfile(user, null);
    }
}