package com.camcruzt.backgrounds.listentotheart.widget;

import android.app.IntentService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.support.annotation.Nullable;

import com.camcruzt.backgrounds.listentotheart.model.Artwork;

import java.util.List;

public class ArtworkService extends IntentService {

    public static final String ACTION_UPDATE_ARTWORK_IMAGE =
            "android.appwidget.action.update_image";

    public static final String UPDATE_WIDGET =
            "android.appwidget.action.ACTION_UPDATE_WIDGET_BROADCAST";

    public static final String ARTWORK_KEY = "artwork";

    public ArtworkService() {
        super("ArtworkService");
    }

    public static void startActionUpdateWidgetArtwork(Context context, Artwork artwork){
        Intent intent = new Intent(context, ArtworkService.class);
        intent.setAction(ACTION_UPDATE_ARTWORK_IMAGE);
        intent.putExtra(ARTWORK_KEY, artwork);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_UPDATE_ARTWORK_IMAGE.equals(action)) {
                Artwork artwork = intent.getParcelableExtra(ARTWORK_KEY);
                handleActionUpdateWidgetArtwork(artwork);
            }
        }
    }

    private void handleActionUpdateWidgetArtwork(Artwork artwork) {
        Intent intent = new Intent(UPDATE_WIDGET);
        intent.setAction(UPDATE_WIDGET);
        intent.putExtra(ARTWORK_KEY, artwork);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            sendImplicitBroadcast(getBaseContext(), intent);
        } else {
            sendBroadcast(intent);
        }
    }

    //Taken from https://commonsware.com/blog/2017/04/11/android-o-implicit-broadcast-ban.html
    private static void sendImplicitBroadcast(Context ctxt, Intent i) {
        PackageManager pm=ctxt.getPackageManager();
        List<ResolveInfo> matches=pm.queryBroadcastReceivers(i, 0);

        for (ResolveInfo resolveInfo : matches) {
            Intent explicit=new Intent(i);
            ComponentName cn=
                    new ComponentName(resolveInfo.activityInfo.applicationInfo.packageName,
                            resolveInfo.activityInfo.name);

            explicit.setComponent(cn);
            ctxt.sendBroadcast(explicit);
        }
    }

}