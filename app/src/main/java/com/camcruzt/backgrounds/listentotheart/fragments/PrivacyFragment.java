package com.camcruzt.backgrounds.listentotheart.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.camcruzt.backgrounds.listentotheart.R;
import com.camcruzt.backgrounds.listentotheart.adapter.PrivacyRecyclerAdapter;
import com.camcruzt.backgrounds.listentotheart.databinding.FragmentPrivacyBinding;

import java.util.Arrays;
import java.util.List;

public class PrivacyFragment extends BaseFragment {

    private FragmentPrivacyBinding mBinding;
    private PrivacyRecyclerAdapter mAdapter;

    public PrivacyFragment(){
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_privacy, container, false);
        View rootView = mBinding.getRoot();

        String[] privacyTitles = getResources().getStringArray(R.array.privacy_titles);
        String[] privacyContent = getResources().getStringArray(R.array.privacy_content);
        List<String> titleList = Arrays.asList(privacyTitles);
        List<String> contentList = Arrays.asList(privacyContent);
        mBinding.privacyRv.setHasFixedSize(false);
        mAdapter = new PrivacyRecyclerAdapter(getContext(), titleList, contentList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mBinding.privacyRv.setLayoutManager(linearLayoutManager);
        mBinding.privacyRv.setAdapter(mAdapter);


        return rootView;
    }
}
