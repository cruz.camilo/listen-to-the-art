package com.camcruzt.backgrounds.listentotheart.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.camcruzt.backgrounds.listentotheart.R;
import com.camcruzt.backgrounds.listentotheart.adapter.TermsRecyclerAdapter;
import com.camcruzt.backgrounds.listentotheart.databinding.FragmentTermsBinding;

import java.util.Arrays;
import java.util.List;

public class AboutFragment extends BaseFragment {

    public AboutFragment(){
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentTermsBinding mBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_terms, container, false);
        View rootView = mBinding.getRoot();

        mBinding.termsTv.setText(getString(R.string.about_backgrounds));

        CharSequence[] termsContent = getResources().getTextArray(R.array.about_backgrounds);
        List<CharSequence> contentList = Arrays.asList(termsContent);
        mBinding.termsRv.setHasFixedSize(false);
        TermsRecyclerAdapter mAdapter = new TermsRecyclerAdapter(getContext(), contentList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mBinding.termsRv.setLayoutManager(linearLayoutManager);
        mBinding.termsRv.setAdapter(mAdapter);

        return rootView;
    }
}