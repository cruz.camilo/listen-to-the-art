package com.camcruzt.backgrounds.listentotheart.viewmodel;

import android.annotation.TargetApi;
import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;

import com.camcruzt.backgrounds.listentotheart.livedata.FirebaseQueryLiveData;
import com.camcruzt.backgrounds.listentotheart.model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class UserViewModel extends ViewModel {

    private static final String NUMBER_PUBLICATIONS_KEY = "numberPublications";
    private static String TAG = UserViewModel.class.getName();
    public DatabaseReference dataRef =
            FirebaseDatabase.getInstance().getReference().child("users");
    private List<User> mList = new ArrayList<>();
    private FirebaseQueryLiveData liveData;

    @TargetApi(Build.VERSION_CODES.N)
    @RequiresApi(api = Build.VERSION_CODES.N)
    public LiveData<User> getUserById(@NonNull String uId ) {
        liveData = new FirebaseQueryLiveData(dataRef.child(uId));
        LiveData<User> userLiveData =
                Transformations.map(liveData, new Deserializer());
        return userLiveData;
    }

    public void updateCounter(String uId, int amount){
        DatabaseReference userRef = dataRef.child(uId);
        Map<String, Object> userUpdate = new HashMap<>();
        userUpdate.put(NUMBER_PUBLICATIONS_KEY, String.valueOf(amount));
        userRef.updateChildren(userUpdate);
    }

    public void followAccount(String signedUserId, String followedUserId, Boolean alreadyFollowing){
        Map<String, Object> followAccountUpdate = new HashMap<>();
        if (!alreadyFollowing){
            followAccountUpdate.put(followedUserId, "true");
            dataRef.child(signedUserId).child("followedAccounts").updateChildren(followAccountUpdate);
        } else {
            dataRef.child(signedUserId).child("followedAccounts").child(followedUserId).removeValue();
        }
    }

    @NonNull
    public LiveData<List<User>> getUserListLiveData(@NonNull Query query){
        FirebaseQueryLiveData mLiveData = new FirebaseQueryLiveData(query);

        LiveData<List<User>> userListLiveData =
                Transformations.map(mLiveData, new ListDeserializer());

        return userListLiveData;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private class Deserializer implements Function<DataSnapshot, User> {
        @Override
        public User apply(DataSnapshot dataSnapshot) {
            return dataSnapshot.getValue(User.class);
        }
    }

    private class ListDeserializer implements Function<DataSnapshot, List<User>> {
        @Override
        public List<User> apply(DataSnapshot dataSnapshot) {
            mList.clear();
            for(DataSnapshot snap : dataSnapshot.getChildren()){
                User msg = snap.getValue(User.class);
                mList.add(msg);
            }
            return mList;
        }
    }
}