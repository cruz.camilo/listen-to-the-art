package com.camcruzt.backgrounds.listentotheart.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.camcruzt.backgrounds.listentotheart.R;
import com.camcruzt.backgrounds.listentotheart.activities.MainActivity;
import com.camcruzt.backgrounds.listentotheart.databinding.SaveAudioFragmentBinding;
import com.camcruzt.backgrounds.listentotheart.model.Artwork;
import com.camcruzt.backgrounds.listentotheart.utils.Utils;
import com.camcruzt.backgrounds.listentotheart.viewmodel.ArtworkViewModel;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.camcruzt.backgrounds.listentotheart.fragments.ProfileFragment.PARCELABLE_KEY;

public class SaveAudioFragment extends BaseFragment {

    private static final int RC_AUDIO_PICKER = 1;
    private static final int RC_AUDIO_PICKER_2 = 2;
    private static final String DETAIL_AUDIO_URL_KEY = "detailAudioUrl";
    private static final String DETAIL_AUDIO_FILENAME_KEY = "detailAudioFilename";
    private static final String MAIN_AUDIO_URL_KEY = "mainAudioUrl";
    private static final String MAIN_AUDIO_FILENAME_KEY = "mainAudioFilename";
    private static final String ARTWORK_ID_KEY = "artworkIdKey";

    public SaveAudioFragment() {
    }

    private SaveAudioFragmentBinding mBinding;
    private Uri mMainAudioUri;
    private Uri mDetailAudioUri;
    private String mMainAudioUrl;
    private String mDetailAudioUrl;
    private ArtworkViewModel mArtworkViewModel;
    private Artwork mArtwork = new Artwork();
    private String mMainAudioFilename, mDetailAudioFilename;
    private Map<String, Object> audioMap = new HashMap<>();
    private ProgressDialog progressDialog;
    private String mSignedUserUid;
    private String TAG = SaveAudioFragment.class.getName();
    private String mArtworkId;
    private final int PICK_MAIN_AUDIO = 1;
    private final int PICK_ADDITIONAL_AUDIO = 2;


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(ARTWORK_ID_KEY, mArtworkId);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater,
                R.layout.save_audio_fragment, container, false);
        final View rootView = mBinding.getRoot();
        ((MainActivity) getActivity()).updateToolbarTitle(getContext().getString(R.string.add_audio));

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        mSignedUserUid = sharedPref.getString(getString(R.string.user_id_key_sharedpreferences), "");
        mArtworkViewModel = ViewModelProviders.of(this).get(ArtworkViewModel.class);

        Bundle bundle = getArguments();
        if(bundle!=null){
            mArtworkId = bundle.getString(PARCELABLE_KEY);
        } else {
            if (savedInstanceState != null && !savedInstanceState.isEmpty()) {
                mArtworkId = savedInstanceState.getString(ARTWORK_ID_KEY);
            }
        }

        if(mArtworkId !=null){
            getArtworkFromDb(mArtworkId);
        }

        mBinding.mainAudioUploadBtn.setOnClickListener(v -> {

                    if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                        if (getContext().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {

                    /*if (shouldShowRequestPermissionRationale(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    }*/

                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    PICK_MAIN_AUDIO);
                        } else {
                            selectAudio(true);
                        }
                    } else {
                        selectAudio(true);
                    }
        });



        mBinding.additionalAudioUploadBtn.setOnClickListener(v -> {
            if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                if (getContext().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    /*if (shouldShowRequestPermissionRationale(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    }*/

                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            PICK_MAIN_AUDIO);
                } else {
                    selectAudio(false);
                }
            } else {
                selectAudio(false);
            }
        });

        mBinding.audioSaveBtn.setOnClickListener(v -> {
                if(mMainAudioUri!=null){
                    int mainAudioSize = getAudioFileSize(mMainAudioUri);
                    int detailAudioSize = -1;
                    if (mDetailAudioUri!=null){
                        detailAudioSize = getAudioFileSize(mDetailAudioUri);
                    }

                    if(mainAudioSize > 0 && mainAudioSize <= 4200000 && detailAudioSize <= 4200000){
                        showProgressDialog();
                        saveAudioToStorage(mMainAudioUri, mMainAudioFilename);
                    } else if (mainAudioSize > 4200000) {
                        Toast.makeText(getContext(), getContext().getString(R.string.main_audiofile_size_toast), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getContext(), getContext().getString(R.string.detail_audiofile_size_toast), Toast.LENGTH_LONG).show();
                    }
                } else if (mDetailAudioUri!=null){
                    int detailAudioSize = getAudioFileSize(mDetailAudioUri);
                    if(detailAudioSize > 0 && detailAudioSize <= 4200000){
                        if(!progressDialog.isShowing()){
                            showProgressDialog();
                        }
                        saveAudioToStorage(mDetailAudioUri, mDetailAudioFilename);
                    } else if (detailAudioSize > 4200000) {
                        Toast.makeText(getContext(), getContext().getString(R.string.detail_audiofile_size_toast), Toast.LENGTH_LONG).show();
                    }
                }
        });

        mBinding.deleteMainAudioBtn.setOnClickListener(v -> {
            if (mMainAudioUri != null || mArtwork.getMainAudioUrl().startsWith("http")) {
                showDeleteConfirmationDialog(mArtwork.getMainAudioUrl());
                getArtworkFromDb(mArtworkId);

            }
        });

        mBinding.deleteDetailAudioBtn.setOnClickListener(v -> {
            if (mDetailAudioUri != null || mArtwork.getDetailAudioUrl().startsWith("http")) {
                showDeleteConfirmationDialog(mArtwork.getDetailAudioUrl());
                getArtworkFromDb(mArtworkId);
            }
        });

        return rootView;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void getArtworkFromDb(String artworkId) {
        LiveData<Artwork> artworkLiveData = mArtworkViewModel.getDataSnapshotLiveData(artworkId);
        artworkLiveData.observe(this, new Observer<Artwork>() {
            @Override
            public void onChanged(@Nullable Artwork artwork) {
                mArtwork = artwork;
                updateButtonState();
                updateUI();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PICK_MAIN_AUDIO) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                selectAudio(true);
            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.share_permission_msg), Toast.LENGTH_LONG).show();
            }
        }

        if (requestCode == PICK_ADDITIONAL_AUDIO) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                selectAudio(false);
            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.share_permission_msg), Toast.LENGTH_LONG).show();
            }
        }
    }

    public int getAudioFileSize(Uri uri) {
        Cursor cursor = getActivity().getContentResolver()
                .query(uri, null, null, null, null, null);
        int sizeIndex = 0;
        try {
            if (cursor != null && cursor.moveToFirst()) {
                String displayName = cursor.getString(
                        cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                Log.i(TAG, "Display Name: " + displayName);
                sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
                String size = null;
                if (!cursor.isNull(sizeIndex)) {
                    size = cursor.getString(sizeIndex);
                    sizeIndex = Integer.valueOf(size);
                } else {
                    sizeIndex = -1;
                }
                Log.i(TAG, "Size: " + size);
            }
        } finally {
            cursor.close();
        }
        return sizeIndex;
    }

    private void updateButtonState(){
        //Update delete button state
        if(mArtwork.getMainAudioUrl() == null || !mArtwork.getMainAudioUrl().startsWith("http")){
            mBinding.deleteMainAudioBtn.setVisibility(View.GONE);
        } else {
            mBinding.deleteMainAudioBtn.setVisibility(View.VISIBLE);
        }

        if(mArtwork.getDetailAudioUrl() == null || !mArtwork.getDetailAudioUrl().startsWith("http")){
            mBinding.deleteDetailAudioBtn.setVisibility(View.GONE);
        } else {
            mBinding.deleteDetailAudioBtn.setVisibility(View.VISIBLE);
        }

        //Update save button state
        if(mMainAudioUri == null && mDetailAudioUri == null){
            mBinding.audioSaveBtn.setEnabled(false);
        } else {
            mBinding.audioSaveBtn.setEnabled(true);
        }

        //Update upload additional audio button state
        if(mMainAudioUri != null
                || mArtwork.getMainAudioUrl()!=null && mArtwork.getMainAudioUrl().startsWith("http")
                || mArtwork.getDetailAudioUrl()!=null && mArtwork.getDetailAudioUrl().startsWith("http")
                || mDetailAudioUri!=null){
            mBinding.additionalAudioUploadBtn.setEnabled(true);
        } else if(mMainAudioUri==null && mArtwork.getMainAudioUrl()==null || mArtwork.getDetailAudioUrl() == null) {
            mBinding.additionalAudioUploadBtn.setEnabled(false);
        }
    }

    private void updateUI() {
        updateButtonState();

        if(mMainAudioUri == null){
            if (mArtwork.getMainAudioFilename()==null || mArtwork.getMainAudioFilename().isEmpty()) {
                mBinding.mainAudioTv.setText(R.string.no_audio_file);
            } else {
                mMainAudioUrl = mArtwork.getMainAudioUrl();
                mBinding.mainAudioTv.setText(mArtwork.getMainAudioFilename());
            }
        }

        if (mDetailAudioUri == null){
            if (mArtwork.getDetailAudioFilename() == null || mArtwork.getDetailAudioFilename().isEmpty()) {
                mBinding.detailsAudioTv.setText(R.string.no_audio_file);
            } else {
                mDetailAudioUrl = mArtwork.getDetailAudioUrl();
                mBinding.detailsAudioTv.setText(mArtwork.getDetailAudioFilename());
            }
        }
    }

    private void selectAudio(Boolean isMainAudio) {
        // Taken from: http://sudhanshuvinodgupta.blogspot.com/2012/07/using-intentactionpick.html
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
        if (isMainAudio) {
            startActivityForResult(Intent.createChooser(intent, "Complete action using"), RC_AUDIO_PICKER);
        } else {
            startActivityForResult(Intent.createChooser(intent, "Complete action using"), RC_AUDIO_PICKER_2);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_AUDIO_PICKER && resultCode == RESULT_OK) {
            mMainAudioUri = data.getData();
            updateButtonState();
            String audioPath = "";
            if (mMainAudioUri != null) {
                audioPath = Utils.getFilePath(mMainAudioUri, getContext());
            }
            if(audioPath!=null){
                mMainAudioFilename = audioPath.substring(audioPath.lastIndexOf("/") + 1);
                mBinding.mainAudioTv.setText(mMainAudioFilename);
            }
        } else if (requestCode == RC_AUDIO_PICKER_2 && resultCode == RESULT_OK) {
            mDetailAudioUri = data.getData();

            String audioPath = "";
            if (mDetailAudioUri != null) {
                audioPath = Utils.getFilePath(mDetailAudioUri, getContext());
            }
            mDetailAudioFilename = audioPath.substring(audioPath.lastIndexOf("/") + 1);
            mBinding.detailsAudioTv.setText(mDetailAudioFilename);
        }
    }

    private void showProgressDialog(){
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage(getString(R.string.saving_message));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void saveAudioToStorage(Uri audioUri, String filename) {
        mArtworkViewModel.uploadFileToDb(audioUri, mArtwork.getId(), filename, mSignedUserUid, true);
        mArtworkViewModel.getFileUploadIsSuccessful().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean isSuccessful) {
                if (isSuccessful) {
                    if (!mArtworkViewModel.getFileUrl().isEmpty() && mMainAudioUrl == null) {
                        mMainAudioUrl = mArtworkViewModel.getFileUrl();
                        audioMap.put(MAIN_AUDIO_URL_KEY, mMainAudioUrl);
                        audioMap.put(MAIN_AUDIO_FILENAME_KEY, mMainAudioFilename);
                        if (mDetailAudioUri != null) {
                            saveAudioToStorage(mDetailAudioUri, mDetailAudioFilename);
                            mArtworkViewModel.setFileUrl("");
                        } else {
                            saveMapAndClear();
                            progressDialog.dismiss();
                            Toast.makeText(getContext(), getContext().getString(R.string.saved_toast), Toast.LENGTH_LONG).show();
                            returnToProfileScreen(3);
                        }
                    } else if (!mArtworkViewModel.getFileUrl().isEmpty() && !mArtworkViewModel.getFileUrl().equals(mMainAudioUrl)) {
                        mDetailAudioUrl = mArtworkViewModel.getFileUrl();
                        audioMap.put(DETAIL_AUDIO_URL_KEY, mDetailAudioUrl);
                        audioMap.put(DETAIL_AUDIO_FILENAME_KEY, mDetailAudioFilename);
                        saveMapAndClear();
                        progressDialog.dismiss();
                        mArtworkViewModel.setFileUrl("");
                        Toast.makeText(getContext(), getContext().getString(R.string.saved_toast), Toast.LENGTH_LONG).show();
                        returnToProfileScreen(3);
                    }
                }
            }
        });
    }

    private void showDeleteConfirmationDialog(String audioUrl) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(R.string.delete_dialog_audio);
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                deleteAudioFromStorage(audioUrl);
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void deleteAudioFromStorage(String audioUrl) {
        StorageReference photoRef = mArtworkViewModel.mFirebaseStorage.getReferenceFromUrl(audioUrl);
        photoRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // File deleted successfully
                deleteAudioFromDb(audioUrl);
                updateUI();
                Toast.makeText(getContext(), getString(R.string.audio_deleted), Toast.LENGTH_SHORT).show();
                if(audioUrl.equals(mArtwork.getMainAudioUrl())){
                    mBinding.mainAudioTv.setText(R.string.no_audio_file);
                } else {
                    mBinding.detailsAudioTv.setText(R.string.no_audio_file);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // An error occurred!
                Log.d(TAG, "onFailure: did not delete file");
            }
        });
    }

    private void deleteAudioFromDb(String audioUrl) {
        if (audioUrl.equals(mArtwork.getMainAudioUrl())) {
            audioMap.put(MAIN_AUDIO_FILENAME_KEY, "");
            audioMap.put(MAIN_AUDIO_URL_KEY, "");
            saveMapAndClear();
        } else if (audioUrl.equals(mArtwork.getDetailAudioUrl())) {
            audioMap.put(DETAIL_AUDIO_FILENAME_KEY, "");
            audioMap.put(DETAIL_AUDIO_URL_KEY, "");
            saveMapAndClear();
        }
        updateUI();
    }

    private void saveMapAndClear() {
        mArtworkViewModel.saveAudioToDb(mArtwork.getId(), audioMap);
        audioMap.clear();
    }
}