package com.camcruzt.backgrounds.listentotheart.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.camcruzt.backgrounds.listentotheart.R;
import com.camcruzt.backgrounds.listentotheart.model.Artwork;

import java.util.List;

public class ArtworkRecylerAdapter extends RecyclerView.Adapter<ArtworkRecylerAdapter.ArtworkViewHolder> {

    private Context context;
    private List<Artwork> mArtworks;
    final private ListItemClickListener mOnClickListener;

    public ArtworkRecylerAdapter(Context context, List<Artwork> artworks, ListItemClickListener mOnClickListener) {
        this.context = context;
        this.mArtworks = artworks;
        this.mOnClickListener = mOnClickListener;
    }

    class ArtworkViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView artworkImageView;
        public ArtworkViewHolder(@NonNull View itemView) {
            super(itemView);
            artworkImageView = (ImageView)itemView.findViewById(R.id.artworkItemImageView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            View view = v;
            mOnClickListener.onArtworkClick(clickedPosition, v);
        }
    }

    @NonNull
    @Override
    public ArtworkViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        int artworkItem = R.layout.item_artwork;
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(artworkItem, viewGroup, false);
        ArtworkViewHolder viewHolder = new ArtworkViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ArtworkViewHolder holder, int position) {
        Artwork currentArtwork = mArtworks.get(position);

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .override(300,300)
                .error(R.drawable.free_user_icon_21);

        Glide.with(context)
                .load(currentArtwork.getThumbnailUrl())
                .apply(options)
                .into(holder.artworkImageView);
    }

    @Override
    public int getItemCount() {
        return mArtworks.size();
    }

    public void setData(List<Artwork> data) {
        if (data != null && !data.isEmpty()) {
            mArtworks.addAll(data);
            notifyDataSetChanged();
        }
    }

    public void clear() {
        int size = getItemCount();
        if (size > 0) {
            mArtworks.clear();
            notifyDataSetChanged();
        }
    }

    public void add(Artwork artwork){
        mArtworks.add(0, artwork);
        notifyDataSetChanged();
    }

    public void addArt(Artwork artwork){
        mArtworks.add(artwork);
    }

    public void addToEnd(Artwork artwork){
        mArtworks.add(artwork);
        notifyDataSetChanged();
    }

    public int find(String artworkId){
        int position = 0;
        for (int i = 0; i< mArtworks.size(); i++){
            if (mArtworks.get(i).getId() == artworkId){
                position = i;
            } else {
                position = -1;
            }
        }
        return position;
    }

    public Boolean hasBeenAdded(String artworkId){
        boolean artworkHasBeenAdded = false;
        for (int i = 0; i< mArtworks.size(); i++){
            if (mArtworks.get(i).getId().equals(artworkId)){
                artworkHasBeenAdded = true;
            }
        }
        return artworkHasBeenAdded;
    }


    public Artwork get(int position){
        return mArtworks.get(position);
    }

    public long getLastItemTimestamp() {
        return mArtworks.get(mArtworks.size() - 1).getTimestampCreatedLong();
    }

    public void swapItems(int itemAIndex) {
        //make sure to check if dataset is null and if itemA and itemB are valid indexes.
        Artwork itemA = mArtworks.get(itemAIndex);
        mArtworks.add(itemA);
        mArtworks.remove(0);
        notifyDataSetChanged();
    }

    public interface ListItemClickListener {
        void onArtworkClick(int clickedItemIndex, View view);
    }
}
