package com.camcruzt.backgrounds.listentotheart.audio;

import android.app.Service;
import android.content.Intent;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import com.camcruzt.backgrounds.listentotheart.R;

import java.io.IOException;
import java.util.ArrayList;

public class AudioPlayerService extends Service implements MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener,MediaPlayer.OnCompletionListener {

    //Based on https://code.tutsplus.com/tutorials/create-a-music-player-on-android-song-playback--mobile-22778
    private MediaPlayer mp;
    private ArrayList<String> audios;
    private int trackSelected;
    private final IBinder musicBind = new AudioBinder();
    private int initPosition = 0;
    private String TAG = AudioPlayerService.class.getName();

    public AudioPlayerService() {
    }

    public void onCreate(){
        super.onCreate();
        trackSelected = 0;
        mp = new MediaPlayer();
        initAudioPlayer();
    }

    public void initAudioPlayer(){
        mp.setWakeMode(getApplicationContext(),
                PowerManager.PARTIAL_WAKE_LOCK);
        mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mp.setOnPreparedListener(this);
        mp.setOnCompletionListener(this);
        mp.setOnErrorListener(this);
        mp.setLooping(false);
    }

    public void setAudioList(ArrayList<String> urlList){
        audios = urlList;
    }

    public class AudioBinder extends Binder {
        public AudioPlayerService getService() {
            return AudioPlayerService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return musicBind;
    }

    @Override
    public boolean onUnbind(Intent intent){
        mp.stop();
        mp.release();
        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if(getPos()>1 && trackSelected==0&&audios.size()>1){
            playNext();
        }
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        mp.reset();
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.seekTo(initPosition);
        mp.start();
    }

    public void playAudio(){
        mp.reset();
        String audioUrl = audios.get(trackSelected);

        try{
            mp.setDataSource(audioUrl);
        } catch (IOException e){
            Log.e(TAG, Resources.getSystem().getString((R.string.error_data_source)), e);
        }

        try{
            mp.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stopPlaying() {
        if (mp != null) {
            mp.stop();
        }
    }

    public int getPos(){
        return mp.getCurrentPosition();
    }

    public int getDuration(){
        return mp.getDuration();
    }

    public boolean isPlaying(){
        return mp.isPlaying();
    }

    public void pausePlayer(){
       mp.pause();
    }

    public void seek(int pos){
        mp.seekTo(pos);
    }

    public void start(){
        mp.start();
    }

    public void playPrev(){
        if(trackSelected >0){
            trackSelected--;
            initPosition = 0;
            playAudio();
        }
    }

    public void playNext(){
        if(trackSelected == 0 && audios.size()>1){
            trackSelected++;
            initPosition = 0;
            playAudio();
        }
    }

    public void setTrackSelected(int trackNumber){
        trackSelected = trackNumber;
    }

    public int getTrackSelected(){
        return trackSelected;
    }

    public void setInitPosition(int position){
        initPosition = position;
    }
}
