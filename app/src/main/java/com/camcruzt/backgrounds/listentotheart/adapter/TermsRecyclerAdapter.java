package com.camcruzt.backgrounds.listentotheart.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.camcruzt.backgrounds.listentotheart.R;

import java.util.List;

public class TermsRecyclerAdapter extends RecyclerView.Adapter<TermsRecyclerAdapter.PrivacyViewHolder> {

    private Context context;
    private List<CharSequence> mContentList;

    public TermsRecyclerAdapter(Context context, List<CharSequence> content) {
        this.context = context;
        this.mContentList = content;
    }

    class PrivacyViewHolder extends RecyclerView.ViewHolder{
        TextView content;
        public PrivacyViewHolder(@NonNull View itemView) {
            super(itemView);
            content = (TextView) itemView.findViewById(R.id.terms_content_jv);
        }
    }

    @NonNull
    @Override
    public PrivacyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        int itemTerms = R.layout.item_terms;
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(itemTerms, viewGroup, false);
        PrivacyViewHolder viewHolder = new PrivacyViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PrivacyViewHolder holder, int position) {
        holder.content.setText(mContentList.get(position));
    }

    @Override
    public int getItemCount() {
        return mContentList.size();
    }

}