package com.camcruzt.backgrounds.listentotheart.fragments;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.camcruzt.backgrounds.listentotheart.R;
import com.camcruzt.backgrounds.listentotheart.activities.MainActivity;
import com.camcruzt.backgrounds.listentotheart.adapter.HomeArtworkRecyclerAdapter;
import com.camcruzt.backgrounds.listentotheart.model.Artwork;
import com.camcruzt.backgrounds.listentotheart.model.Report;
import com.camcruzt.backgrounds.listentotheart.model.User;
import com.camcruzt.backgrounds.listentotheart.viewmodel.ArtworkViewModel;
import com.camcruzt.backgrounds.listentotheart.viewmodel.UserViewModel;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


public class HomeFragment extends BaseFragment implements HomeArtworkRecyclerAdapter.ListItemClickListener,
        HomeArtworkRecyclerAdapter.ReportClickListener, HomeArtworkRecyclerAdapter.AuthorClickListener {

    public static final String TIMESTAMP_KEY = "timestamp";
    private static final String SAVED_ARTWORKS = "adapterList";

    public HomeFragment() {
    }

    private UserViewModel mUserViewModel;
    private User mSignedUser;
    private HashMap<String, String> followedAccounts;
    private String mSignedUserUid;
    public HomeArtworkRecyclerAdapter mHomeAdapter;
    private TextView emptyView;
    private boolean mIsLoading = false;
    private int mArtworksPerPage = 10;
    private int totalItems = 6;
    private DatabaseReference artworkDataRef;
    private long lastNodeID = 0;
    private ArtworkViewModel artworkViewModel;
    private List<Artwork> artworksList;
    private LinearLayoutManager mLinearLayoutManager;
    private RecyclerView mRecyclerView;
    private Parcelable mLayoutSavedState;
    private long lastItemTimestamp = 0;
    private String reportContent = "";
    private Report report;
    private CharSequence[] dialogOptions;
    private Artwork reportedArtwork;
    private String mSignedUserId;
    private DatabaseReference mReportDatabaseReference;
    private AlertDialog alertDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        mSignedUserId = sharedPref.getString(getString(R.string.user_id_key_sharedpreferences), "");
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        ((MainActivity) getActivity()).updateToolbarTitle(getContext().getString(R.string.home));
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        mSignedUserUid = sharedPref.getString(getString(R.string.user_id_key_sharedpreferences), "");
        boolean userHasJustLoggedIn = sharedPref.getBoolean(getString(R.string.just_logged_in_shared_preferences), true);
        mUserViewModel = ViewModelProviders.of(this).get(UserViewModel.class);


        FirebaseDatabase firebaseDatabase = ((MainActivity) getActivity()).mFirebaseDatabase;
        mReportDatabaseReference = firebaseDatabase.getReference().child("reports");

        mRecyclerView = view.findViewById(R.id.homeRecyclerView);

        if (mHomeAdapter == null || userHasJustLoggedIn) {
            sharedPref.edit().putBoolean(getString(R.string.just_logged_in_shared_preferences), false).apply();
            mHomeAdapter = new HomeArtworkRecyclerAdapter(getContext(), new ArrayList<Artwork>(),
                    this, this, this);
        }
        mRecyclerView.setAdapter(mHomeAdapter);

        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        emptyView = (TextView) view.findViewById(R.id.emptyHome);

        if (searchView != null) {
            setSearchViewVisibility(false);
        }

        if (!mSignedUserUid.equals("")) {
            getSignedUser();
        }

        if (isAdded()) {
            dialogOptions = getResources().getTextArray(R.array.inappropriate_content);
        }

        artworkViewModel = ViewModelProviders.of(this).get(ArtworkViewModel.class);
        artworkDataRef = artworkViewModel.dataRef;

        if (searchView != null) {
            setSearchViewVisibility(false);
        }

        //Taken from https://blog.shajeelafzal.com/2017/12/13/firebase-realtime-database-pagination-guide-using-recyclerview/
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int totalItemCount = mLinearLayoutManager.getItemCount();
                int lastVisible = mLinearLayoutManager.findLastCompletelyVisibleItemPosition();

                boolean endHasBeenReached = lastVisible + 1 >= totalItemCount;
                if (!mIsLoading && totalItemCount > 0 && endHasBeenReached
                        && mLinearLayoutManager.findFirstCompletelyVisibleItemPosition() != 0) {
                    Log.d("Home", "end reached!");
                    getArtworks(mHomeAdapter.getLastItemTimestamp());
                    mIsLoading = true;
                }
            }
        });

        return view;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (isAdded()) {
            getActivity().getMenuInflater().inflate(R.menu.artwork_menu, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.reportOption) {
            showReportDialog(reportedArtwork.getId());
            return true;
        }
        return super.onContextItemSelected(item);
    }

    private void showReportDialog(String artworkId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getContext().getResources().getString(R.string.report_dialog_title));
        builder.setSingleChoiceItems(dialogOptions, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        reportContent = "Sex";
                        break;
                    case 1:
                        reportContent = "Violence";
                        break;
                    case 2:
                        reportContent = "Bullying";
                        break;
                    case 3:
                        reportContent = "Drugs";
                        break;
                }
                report = new Report(mSignedUserId, artworkId, reportContent);
            }
        });

        builder.setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (reportContent != null) {
                    String databaseKey = mReportDatabaseReference.push().getKey();
                    mReportDatabaseReference.child(databaseKey).setValue(report);
                    Toast toast = Toast.makeText(getContext(), getContext().getResources().getString(R.string.thanks_message), Toast.LENGTH_LONG);
                    LinearLayout layout = (LinearLayout) toast.getView();
                    if (layout.getChildCount() > 0) {
                        TextView tv = (TextView) layout.getChildAt(0);
                        tv.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
                    }
                    toast.show();
                }
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        alertDialog = builder.create();
        alertDialog.show();
    }


    private void getArtworks(long nodeID) {
        Query searchQuery;
        if (nodeID == 0) {
            searchQuery = artworkDataRef
                    .orderByChild(TIMESTAMP_KEY)
                    .limitToLast(mArtworksPerPage);
            mHomeAdapter.clear();
        } else {
            searchQuery = artworkDataRef
                    .orderByChild(TIMESTAMP_KEY)
                    .endAt(nodeID)
                    .limitToLast(mArtworksPerPage);
        }
        if (lastNodeID == 0 || lastNodeID != nodeID) {
            lastNodeID = nodeID;
        } else {
            return;
        }

        LiveData<List<Artwork>> artworkListLiveData = artworkViewModel.getArtworkListLiveData(searchQuery);
        artworkListLiveData.observe(this, new Observer<List<Artwork>>() {
            @Override
            public void onChanged(@Nullable List<Artwork> artworks) {
                artworksList = artworks;
                Collections.reverse(artworksList);
                addArtworksToAdapter(artworks);
            }
        });

        if (followedAccounts.size() == 1 && mSignedUser.getNumberPublications() == "0") {
            emptyView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.INVISIBLE);
        }
    }

    private void addArtworksToAdapter(@NonNull List<Artwork> artworks) {
        for (int i = 0; i < artworks.size(); i++) {
            if (followedAccounts.containsKey(artworks.get(i).getAuthorId())) {
                if (!mHomeAdapter.hasBeenAdded(artworks.get(i).getId())) {
                    mHomeAdapter.add(artworks.get(i));
                }
            }
            if (mHomeAdapter.getItemCount() == totalItems) {
                totalItems += totalItems;
                break;
            } else if (mHomeAdapter.getItemCount() < totalItems && i == mArtworksPerPage - 1) {
                if (lastItemTimestamp == 0 || lastItemTimestamp != artworks.get(i).getTimestampCreatedLong()) {
                    lastItemTimestamp = artworks.get(i).getTimestampCreatedLong();
                    getArtworks(lastItemTimestamp);
                } else {
                    break;
                }
            }
        }

        if (mHomeAdapter.getItemCount() > 2 && mHomeAdapter.get(0).getTimestampCreatedLong() < mHomeAdapter.get(1).getTimestampCreatedLong()) {
            mHomeAdapter.swapItems(0);
        }
        if (mLayoutSavedState != null) {
            mRecyclerView.getLayoutManager().onRestoreInstanceState(mLayoutSavedState);
        }
        mHomeAdapter.notifyDataSetChanged();
        mIsLoading = false;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void getSignedUser() {
        LiveData<User> userLiveData = mUserViewModel.getUserById(mSignedUserUid);
        userLiveData.observe(this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                if (user != null) {
                    mSignedUser = user;
                    followedAccounts = mSignedUser.getFollowedAccounts();
                    if (mHomeAdapter.getItemCount() == 0) {
                        getArtworks(0);
                    }
                }
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        mLayoutSavedState = mLinearLayoutManager.onSaveInstanceState();
    }

    @Override
    public void onArtworkClick(int clickedItemIndex, View view) {
        Artwork selectedArtwork = mHomeAdapter.get(clickedItemIndex);
        switchToLargeArtworkFragment(selectedArtwork, view);
    }

    @Override
    public void onReportClick(int clickedItemIndex, View view) {
        registerForContextMenu(view);
        reportedArtwork = mHomeAdapter.get(clickedItemIndex);
        getActivity().openContextMenu(view);
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onAuthorClick(int clickedItemIndex, View view) {
        Artwork artwork = mHomeAdapter.get(clickedItemIndex);
        String authorId = artwork.getAuthorId();
        showUserProfile(null, authorId);
    }
}