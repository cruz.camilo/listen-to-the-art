package com.camcruzt.backgrounds.listentotheart.audio;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.widget.MediaController;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class AudioController extends MediaController {

    public AudioController(Context c){
        super(c);
    }

    @Override
    public void show(int timeout) {
        super.show(7000);
    }
}
