package com.camcruzt.backgrounds.listentotheart.adapter;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.ObjectKey;
import com.camcruzt.backgrounds.listentotheart.R;
import com.camcruzt.backgrounds.listentotheart.activities.MainActivity;
import com.camcruzt.backgrounds.listentotheart.model.Artwork;
import com.camcruzt.backgrounds.listentotheart.model.User;
import com.camcruzt.backgrounds.listentotheart.utils.Utils;
import com.camcruzt.backgrounds.listentotheart.viewmodel.UserViewModel;

import java.util.ArrayList;
import java.util.List;

public class HomeArtworkRecyclerAdapter extends RecyclerView.Adapter<HomeArtworkRecyclerAdapter.ArtworkViewHolder> {

    private Context context;
    private ArrayList<Artwork> mArtworks;
    final private ListItemClickListener mOnClickListener;
    final private ReportClickListener mOnReportListener;
    final private AuthorClickListener mOnAuthorListener;
    private UserViewModel userViewModel;

    public HomeArtworkRecyclerAdapter(Context context, ArrayList<Artwork> artworks,
                                      ListItemClickListener mOnClickListener,
                                      ReportClickListener mOnReportListener,
                                      AuthorClickListener mOnAuthorListener) {
        this.context = context;
        this.mArtworks = artworks;
        this.mOnClickListener = mOnClickListener;
        this.mOnReportListener = mOnReportListener;
        this.mOnAuthorListener = mOnAuthorListener;
    }

    class ArtworkViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView artworkIv;
        ImageView profilePhotoIv;
        TextView authorName;
        TextView publicationDate;
        TextView report;

        public ArtworkViewHolder(@NonNull View itemView) {
            super(itemView);
            artworkIv = (ImageView) itemView.findViewById(R.id.artworkItemImageView);
            report = (TextView) itemView.findViewById(R.id.reportButton);
            profilePhotoIv = (ImageView) itemView.findViewById(R.id.profilePhotoMainScreen);
            authorName = (TextView) itemView.findViewById(R.id.usernameMainScreenTv);
            publicationDate = (TextView) itemView.findViewById(R.id.timestampMainScreenTv);
            artworkIv.setOnClickListener(this);
            authorName.setOnClickListener(this);
            report.setOnClickListener(this);
        }

        // https://stackoverflow.com/questions/36759744/
        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            if (v.getId() == report.getId()) {
                mOnReportListener.onReportClick(clickedPosition, v);
            } else if (v.getId() == authorName.getId()) {
                mOnAuthorListener.onAuthorClick(clickedPosition, v);
            } else {
                mOnClickListener.onArtworkClick(clickedPosition, v);
            }
        }
    }

    @NonNull
    @Override
    public ArtworkViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        int artworkItem = R.layout.item_artwork_main_screen;
        LayoutInflater inflater = LayoutInflater.from(context);

        userViewModel = ViewModelProviders.of(((MainActivity) context)).get(UserViewModel.class);

        View view = inflater.inflate(artworkItem, viewGroup, false);
        ArtworkViewHolder viewHolder = new ArtworkViewHolder(view);
        return viewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(@NonNull ArtworkViewHolder holder, int position) {
        Artwork currentArtwork = mArtworks.get(position);

        RequestOptions artOptions = new RequestOptions()

                .signature(new ObjectKey(currentArtwork.getTimestampCreatedLong()))
                .centerCrop()
                .override(1080, 950)
                .placeholder(new ColorDrawable(Color.WHITE))
                .error(R.drawable.free_user_icon_21);

        RequestOptions userOptions = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .override(150, 150)
                .circleCrop()
                .error(R.drawable.free_user_icon_21);

        //Load artwork image
        Glide.with(context)
                .load(currentArtwork.getThumbnailUrl())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                        String dateString = Utils.timestampToDateString(currentArtwork.getTimestampCreatedLong());

                        LiveData<User> user = userViewModel.getUserById(currentArtwork.getAuthorId());
                        user.observe(((MainActivity) context), new Observer<User>() {
                            @Override
                            public void onChanged(@Nullable User user) {
                                if (user != null) {
                                    holder.authorName.setText(user.getUsername());

                                    //Load profile image
                                    Glide.with(context)
                                            .load(user.getPhotoUrl())
                                            .apply(userOptions)
                                            .into(holder.profilePhotoIv);
                                }
                            }
                        });

                        if (dateString != null) {
                            holder.publicationDate.setText(dateString);
                        }
                        return false;
                    }
                })
                .apply(artOptions)
                .into(holder.artworkIv);
    }

    @Override
    public int getItemCount() {
        return mArtworks.size();
    }

    public void setData(List<Artwork> data) {
        if (data != null && !data.isEmpty()) {
            mArtworks.clear();
            mArtworks.addAll(data);
            notifyDataSetChanged();
        }
    }

    public void clear() {
        int size = getItemCount();
        if (size > 0) {
            mArtworks.clear();
            notifyDataSetChanged();
        }
    }

    public void add(Artwork artwork) {
        mArtworks.add(artwork);
    }

    public Boolean hasBeenAdded(String artworkId) {
        boolean artworkHasBeenAdded = false;
        for (int i = 0; i < mArtworks.size(); i++) {
            if (mArtworks.get(i).getId().equals(artworkId)) {
                artworkHasBeenAdded = true;
            }
        }
        return artworkHasBeenAdded;
    }

    public long getLastItemTimestamp() {
        return mArtworks.get(mArtworks.size() - 1).getTimestampCreatedLong();
    }

    public Artwork get(int position) {
        return mArtworks.get(position);
    }

public interface ListItemClickListener {
    void onArtworkClick(int clickedItemIndex, View view);
}

public interface ReportClickListener {
    void onReportClick(int clickedItemIndex, View view);
}

public interface AuthorClickListener {
    void onAuthorClick(int clickedItemIndex, View view);

}
    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public void swapItems(int itemAIndex) {
        Artwork itemA = mArtworks.get(itemAIndex);
        mArtworks.add(itemA);
        mArtworks.remove(0);
        notifyDataSetChanged();
    }
}