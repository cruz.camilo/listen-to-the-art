package com.camcruzt.backgrounds.listentotheart.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.camcruzt.backgrounds.listentotheart.R;
import com.camcruzt.backgrounds.listentotheart.activities.MainActivity;
import com.camcruzt.backgrounds.listentotheart.databinding.FragmentShareBinding;
import com.camcruzt.backgrounds.listentotheart.model.Artwork;
import com.camcruzt.backgrounds.listentotheart.model.Tag;
import com.camcruzt.backgrounds.listentotheart.model.User;
import com.camcruzt.backgrounds.listentotheart.utils.Utils;
import com.camcruzt.backgrounds.listentotheart.viewmodel.ArtworkViewModel;
import com.camcruzt.backgrounds.listentotheart.viewmodel.UserViewModel;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.storage.StorageReference;
import com.plumillonforge.android.chipview.Chip;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.camcruzt.backgrounds.listentotheart.fragments.ProfileFragment.PARCELABLE_KEY;
import static com.camcruzt.backgrounds.listentotheart.utils.Utils.EXAMPLE_URL;
import static java.text.DateFormat.getDateTimeInstance;

public class ShareFragment extends BaseFragment {

    private static final String TITLE_KEY = "title";
    private static final String DETAILS_KEY = "additionalInfo";
    private static final String DESCRIPTION_KEY = "description";
    private static final String WIDTH_KEY = "width";
    private static final String HEIGHT_KEY = "height";
    private static final String YEAR_KEY = "year";
    private static final String MEDIUM_KEY = "medium";
    private static final String IMAGE_URL_KEY = "imageUrl";
    public static final String HORIZONTAL_KEY = "Horizontal";
    public static final String VERTICAL_KEY = "Vertical";
    private static final String IMAGE_ORIENTATION_KEY = "imageOrientation";
    private static final String SQUARE_KEY = "Square";
    private static final String THUMBNAIL_URL_KEY = "thumbnailUrl";
    private static final String AUTHOR_KEY = "authorUsername";
    private static final String ACCEPTS_TERMS_KEY = "acceptsTerms";
    public User mCurrentUser;
    private DatabaseReference mArtworkDatabaseReference;
    private FragmentShareBinding mBinding;
    private static final String TAG = ShareFragment.class.getName();
    private static final int RC_PHOTO_PICKER = 2;

    private int mNumberPublications;
    private Uri mSelectedImageUri, mThumbnailUri, mResizedImageUri;
    private ArrayAdapter<CharSequence> mSpinnerAdapter;
    private Artwork mArtwork;
    private boolean thereIsKeyword = false;
    private UserViewModel mUserViewModel;
    private ArtworkViewModel artworkViewModel;
    private String title, authorName, year, height, width, description, details, timeStamp, medium;
    private String mImageOrientation;
    private List<Chip> chipList = new ArrayList<>();
    private String mSignedUserId;
    private String mImageUrl, mThumbnailUrl;
    private boolean mInputsAreValidated;
    public boolean mArtworkHasChanged = false;
    private View.OnTouchListener mTouchListener;
    private Integer mImageWidth, mImageHeight;
    private StorageReference mArtworkStorageRef, mThumbnailStorageRef;
    private String databaseKey;
    private ProgressDialog progressDialog;
    private String mCurrentUsername;
    private Boolean acceptsTerms;
    private SharedPreferences sharedPref;
    private int PICK_FROM_GALLERY = 1;

    public ShareFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        mSignedUserId = sharedPref.getString(getString(R.string.user_id_key_sharedpreferences), "");
        acceptsTerms = sharedPref.getBoolean(ACCEPTS_TERMS_KEY, false);

        mBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_share, container, false);
        final View rootView = mBinding.getRoot();

        setSpinnerOptions();
        if (searchView != null) {
            setSearchViewVisibility(false);
        }

        if (Utils.isOnline(getContext())) {
            // Get Database and storage references
            FirebaseDatabase firebaseDatabase = ((MainActivity) getActivity()).mFirebaseDatabase;
            mArtworkDatabaseReference = firebaseDatabase.getReference().child("artworks");

            mUserViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
            artworkViewModel = ViewModelProviders.of(this).get(ArtworkViewModel.class);

            Bundle bundle = getArguments();
            //If true, we are editing on edit fragment screen
            if (bundle != null && bundle.containsKey(PARCELABLE_KEY)) {
                mArtwork = (Artwork) bundle.getParcelable(PARCELABLE_KEY);
                ((MainActivity) getActivity()).
                        updateToolbarTitle(getContext().getString(R.string.edit_artwork));
                updateUI();
                mBinding.inputArtworkInfo.saveBtn.setVisibility(View.GONE);
                // If there's no parcelable, we are on save Artwork screen.
            } else if (bundle == null) {
                ((MainActivity) getActivity()).updateToolbarTitle(getContext().getString(R.string.share));
                returnCurrentUserFromDb();
            }
        }

        mBinding.inputArtworkInfo.saveBtn.setOnClickListener(v -> {
            if (Utils.isOnline(getContext())) {
                saveUpdateArtwork();
            } else {
                Toast toast = Toast.makeText(getContext(), getString(R.string.no_internet_message), Toast.LENGTH_SHORT);
                TextView toastMessage = (TextView) toast.getView().findViewById(android.R.id.message);
                if (toastMessage != null) toastMessage.setGravity(Gravity.CENTER);
                toast.show();
            }
        });

        mBinding.artworkImageView.setOnClickListener(v -> {
            if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                if (getContext().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    // Should we show an explanation?
                    if (shouldShowRequestPermissionRationale(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        // Explain to the user why we need to read the contacts
                    }

                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            PICK_FROM_GALLERY);
                    return;
                } else {
                    openGallery();
                    Utils.hideKeyboardFrom(getContext(), v);
                }
            } else {
                openGallery();
                Utils.hideKeyboardFrom(getContext(), v);
            }

        });

        mBinding.inputArtworkInfo.artworkMediumSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == parent.getCount() - 1) {
                    mBinding.inputArtworkInfo.artworkMediumEditText.setVisibility(View.VISIBLE);
                } else {
                    mBinding.inputArtworkInfo.artworkMediumEditText.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        mBinding.inputArtworkInfo.artworkKeywordsEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                if (charSequence.toString().trim().length() > 0) {
                    //Taken from https://code.i-harness.com/en/q/1330952
                    mBinding.inputArtworkInfo.enterKeywordBtn.
                            setColorFilter(ContextCompat.getColor(getContext(), R.color.holo_blue_dark));
                    thereIsKeyword = true;
                } else {
                    mBinding.inputArtworkInfo.enterKeywordBtn.
                            setColorFilter(ContextCompat.getColor(getContext(), R.color.grey));
                    thereIsKeyword = false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mBinding.inputArtworkInfo.enterKeywordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (thereIsKeyword) {
                    chipList.add(new Tag(mBinding.inputArtworkInfo.artworkKeywordsEditText.getText().toString()));
                    mBinding.inputArtworkInfo.chipview.setChipList(chipList);
                    mBinding.inputArtworkInfo.artworkKeywordsEditText.setText("");
                }
            }
        });

        mBinding.inputArtworkInfo.chipview.setChipLayoutRes(R.layout.chip_close);
        mBinding.inputArtworkInfo.chipview.setOnChipClickListener(chip
                -> mBinding.inputArtworkInfo.chipview.remove(chip));

        mTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mArtworkHasChanged = true;
                return false;
            }
        };


        rootView.setOnTouchListener(mTouchListener);
        setOnTouchListener();

        mBinding.inputArtworkInfo.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioYes:
                        mBinding.inputArtworkInfo.artworkAuthorEditText.setText(mCurrentUsername);
                        break;
                    case R.id.radioNo:
                        mBinding.inputArtworkInfo.artworkAuthorEditText.setText("");
                        break;
                }
            }
        });

        mTouchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mArtworkHasChanged = true;
                return false;
            }
        };


        rootView.setOnTouchListener(mTouchListener);
        setOnTouchListener();

        return rootView;
    }

    private void setAcceptTermsVisibility() {
        if (!acceptsTerms && mNumberPublications < 1) {
            mBinding.inputArtworkInfo.termsCb.setVisibility(View.VISIBLE);
            mBinding.inputArtworkInfo.agreeTermsTv.setVisibility(View.VISIBLE);
            mBinding.inputArtworkInfo.termsTv.setVisibility(View.VISIBLE);
            mBinding.inputArtworkInfo.saveBtn.setEnabled(false);

            mBinding.inputArtworkInfo.termsCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (!isChecked) {
                        mBinding.inputArtworkInfo.saveBtn.setEnabled(false);
                    } else {
                        mBinding.inputArtworkInfo.saveBtn.setEnabled(true);
                    }
                }
            });

            mBinding.inputArtworkInfo.termsTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((MainActivity) getActivity()).replaceFragment(new TermsFragment(), null);
                }
            });
        } else {
            mBinding.inputArtworkInfo.termsCb.setVisibility(View.GONE);
            mBinding.inputArtworkInfo.agreeTermsTv.setVisibility(View.GONE);
            mBinding.inputArtworkInfo.termsTv.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PICK_FROM_GALLERY) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openGallery();
            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.share_permission_msg), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void setOnTouchListener() {
        mBinding.inputArtworkInfo.artworkKeywordsEditText.setOnTouchListener(mTouchListener);
        mBinding.inputArtworkInfo.artworkTitleEditText.setOnTouchListener(mTouchListener);
        mBinding.inputArtworkInfo.artworkYearEditText.setOnTouchListener(mTouchListener);
        mBinding.inputArtworkInfo.artworkHeightEditText.setOnTouchListener(mTouchListener);
        mBinding.inputArtworkInfo.artworkWidthEditText.setOnTouchListener(mTouchListener);
        mBinding.inputArtworkInfo.artworkMediumSpinner.setOnTouchListener(mTouchListener);
        mBinding.inputArtworkInfo.artworkMediumSpinner.setOnTouchListener(mTouchListener);
        mBinding.inputArtworkInfo.artworkDescriptionEditText.setOnTouchListener(mTouchListener);
        mBinding.inputArtworkInfo.artworkDetailsEditText.setOnTouchListener(mTouchListener);
    }

    private void saveUpdateArtwork() {
        getDataFromInputs();

        if (mInputsAreValidated) {
            showProgressDialog();

            //If user has included an image
            if (mSelectedImageUri != null) {
                mThumbnailUri = Utils.getImageUri(getContext(),
                        Utils.getBitmapFromUri(mSelectedImageUri, 800, 800, getContext()));

                mSelectedImageUri = Utils.getImageUri(getContext(),
                        Utils.getBitmapFromUri(mSelectedImageUri, mImageWidth, mImageHeight, getContext()));
            }
            // User saves a new artwork
            if (mArtwork == null) {
                if (!acceptsTerms) {
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putBoolean(ACCEPTS_TERMS_KEY, true);
                    editor.apply();
                }
                databaseKey = mArtworkDatabaseReference.push().getKey();
                uploadPhoto(mSelectedImageUri, null, databaseKey);

            } else {
                //User updates existing artwork
                updateArtwork();
            }
        }
    }

    // Display Image gallery to select photo
    public void openGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, RC_PHOTO_PICKER);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Activity result after selecting image
        if (requestCode == RC_PHOTO_PICKER && resultCode == RESULT_OK) {
            mSelectedImageUri = data.getData();
            String picturePath = "";
            if (mSelectedImageUri != null) {
                picturePath = Utils.getFilePath(mSelectedImageUri, getContext());
                Map<String, Integer> imageDimensions = Utils.getImageWidthAndHeight(mSelectedImageUri, getContext());
                mImageWidth = imageDimensions.get("width");
                mImageHeight = imageDimensions.get("height");
                Log.d(TAG, "Width" + String.valueOf(width));
                Log.d(TAG, "Height" + String.valueOf(height));
                if (mImageWidth > mImageHeight) {
                    mImageOrientation = HORIZONTAL_KEY;
                } else if (mImageWidth.equals(mImageHeight)) {
                    mImageOrientation = SQUARE_KEY;
                } else {
                    mImageOrientation = VERTICAL_KEY;
                }
            }
            RequestOptions options = new RequestOptions()
                    .error(R.drawable.free_user_icon_21);

            Glide.with(this)
                    .load(picturePath)
                    .apply(options).
                    into(mBinding.artworkImageView);
        }
    }

    private void setSpinnerOptions() {
        // Create an ArrayAdapter using the string array and a default spinner layout
        mSpinnerAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.string_array_medium, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        mSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        mBinding.inputArtworkInfo.artworkMediumSpinner.setAdapter(mSpinnerAdapter);
    }

    private void updateUI() {
        Glide.with(this)
                .load(mArtwork.getThumbnailUrl())
                .into(mBinding.artworkImageView);

        mBinding.inputArtworkInfo.artworkTitleEditText.setText(mArtwork.getTitle());
        mBinding.inputArtworkInfo.artworkYearEditText.setText(mArtwork.getYear());
        if (!mArtwork.getAuthorUsername().equals(mCurrentUsername)) {
            mBinding.inputArtworkInfo.radioNo.toggle();
        }
        mBinding.inputArtworkInfo.artworkAuthorEditText.setText(mArtwork.getAuthorUsername());
        mBinding.inputArtworkInfo.artworkHeightEditText.setText(mArtwork.getWidth());
        mBinding.inputArtworkInfo.artworkWidthEditText.setText(mArtwork.getHeight());
        mImageOrientation = mArtwork.getImageOrientation();

        if (mArtwork.getKeywords() != null && mArtwork.getKeywords().size() > 0 && chipList.isEmpty()) {
            for (Map.Entry<String, String> keywords : mArtwork.getKeywords().entrySet()) {
                String key = keywords.getKey();
                chipList.add(new Tag(key));
            }
        }

        if (!chipList.isEmpty()) {
            mBinding.inputArtworkInfo.chipview.setChipList(chipList);
        }

        String[] mMediumArray = getResources().getStringArray(R.array.string_array_medium);

        int spinnerPosition = Arrays.asList(mMediumArray).lastIndexOf(mArtwork.getMedium());

        if (spinnerPosition == -1) {
            spinnerPosition = mSpinnerAdapter.getCount() - 1;
            mBinding.inputArtworkInfo.artworkMediumSpinner.setSelection(spinnerPosition);
            mBinding.inputArtworkInfo.artworkMediumEditText.setVisibility(View.VISIBLE);
            mBinding.inputArtworkInfo.artworkMediumEditText.setText(mArtwork.getMedium());
        } else {
            mBinding.inputArtworkInfo.artworkMediumSpinner.setSelection(spinnerPosition);
        }
        mBinding.inputArtworkInfo.artworkDescriptionEditText.setText(mArtwork.getDescription());
        mBinding.inputArtworkInfo.artworkDetailsEditText.setText(mArtwork.getAdditionalInfo());
    }

    private void getDataFromInputs() {
        mInputsAreValidated = false;
        title = mBinding.inputArtworkInfo.artworkTitleEditText.getText().toString();
        authorName = mBinding.inputArtworkInfo.artworkAuthorEditText.getText().toString();

        year = mBinding.inputArtworkInfo.artworkYearEditText.getText().toString();
        height = mBinding.inputArtworkInfo.artworkHeightEditText.getText().toString();
        width = mBinding.inputArtworkInfo.artworkWidthEditText.getText().toString();
        description = mBinding.inputArtworkInfo.artworkDescriptionEditText.getText().toString();
        details = mBinding.inputArtworkInfo.artworkDetailsEditText.getText().toString();
        timeStamp = getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(new Date());

        if (mBinding.inputArtworkInfo.artworkMediumEditText.getVisibility() == View.INVISIBLE) {
            medium = mBinding.inputArtworkInfo.artworkMediumSpinner.getSelectedItem().toString();
        } else {
            medium = mBinding.inputArtworkInfo.artworkMediumEditText.getText().toString();
        }

        if (mArtwork == null && mSelectedImageUri == null) {
            Toast.makeText(getContext(), getString(R.string.image_validation), Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(title)) {
            Toast.makeText(getContext(), R.string.title_validator, Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(year)) {
            Toast.makeText(getContext(), R.string.year_validator, Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(height)) {
            Toast.makeText(getContext(), R.string.height_validator, Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(width)) {
            Toast.makeText(getContext(), R.string.width_validator, Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(medium) || medium.equals("Select Medium")) {
            Toast.makeText(getContext(), R.string.medium_validator, Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(description)) {
            Toast.makeText(getContext(), R.string.description_validator, Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(authorName)) {
            Toast.makeText(getContext(), R.string.author_validation, Toast.LENGTH_SHORT).show();
        } else {
            mInputsAreValidated = true;
        }
    }

    private void updateArtwork() {
        //Validate if current image is not an example
        if (!mArtwork.getImageUrl().equals(EXAMPLE_URL)) {
            mArtworkStorageRef = ((MainActivity) getActivity()).mFirebaseStorage
                    .getReferenceFromUrl(mArtwork.getImageUrl());
            mThumbnailStorageRef = ((MainActivity) getActivity()).mFirebaseStorage
                    .getReferenceFromUrl(mArtwork.getThumbnailUrl());
        }

        if (mSelectedImageUri != null) {
            uploadPhoto(mSelectedImageUri, null, mArtwork.getId());
        } else {
            updateArtworkDb();
            progressDialog.dismiss();
            returnToProfileScreen(2);
        }
    }

    private void uploadPhoto(Uri imageUri, String filename, String artworkId) {
        artworkViewModel.uploadFileToDb(imageUri, artworkId, filename, mSignedUserId, false);
        artworkViewModel.getFileUploadIsSuccessful().observe(this, isSuccessful -> {
            if (isSuccessful) {
                if (!artworkViewModel.getFileUrl().isEmpty() && mImageUrl == null) {
                    if (imageUri == mSelectedImageUri) {
                        mImageUrl = artworkViewModel.getFileUrl();
                        String thumbnailName = mThumbnailUri.getLastPathSegment() + "_thumbail";
                        if (mArtwork == null) {
                            uploadPhoto(mThumbnailUri, thumbnailName, databaseKey);
                        } else {
                            uploadPhoto(mThumbnailUri, thumbnailName, mArtwork.getId());
                        }
                        artworkViewModel.setFileUrl("");
                    }

                } else if (!artworkViewModel.getFileUrl().isEmpty() &&
                        !artworkViewModel.getFileUrl().equals(mImageUrl) && mThumbnailUrl == null) {

                    mThumbnailUrl = artworkViewModel.getFileUrl();
                    if (mArtwork != null) {
                        updateArtworkDb();
                        deleteTemporaryFiles();
                        if (!mArtwork.getImageUrl().equals(EXAMPLE_URL)) {
                            deleteOldImagesOnStorage();
                        }

                        progressDialog.dismiss();
                        returnToProfileScreen(2);
                    } else {
                        saveToDb();
                    }
                }
            } else {
                Toast.makeText(getActivity(), "Upload failed: ", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //Delete temporary images on phone
    private void deleteTemporaryFiles() {
        ContentResolver contentResolver = getActivity().getContentResolver();

        String thumbnailPath = Utils.getFilePath(mThumbnailUri, getContext());
        contentResolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                MediaStore.Images.ImageColumns.DATA + "=?", new String[]{thumbnailPath});

        String temporaryImagePath = Utils.getFilePath(mSelectedImageUri, getContext());
        contentResolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                MediaStore.Images.ImageColumns.DATA + "=?", new String[]{temporaryImagePath});
    }

    private void deleteOldImagesOnStorage() {
        if (mArtworkStorageRef != null) {
            mArtworkStorageRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.d(TAG, "onSuccess: deleted file");
                }
            });
        }
        if (mThumbnailStorageRef != null) {
            mThumbnailStorageRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.d(TAG, "onSuccess: deleted file");
                }
            });
        }
    }


    private void saveToDb() {
        //Save mArtwork to database
        mArtwork = new Artwork(databaseKey, mSignedUserId, authorName, mCurrentUser.getPhotoUrl(), title, year, height, width, medium,
                mImageUrl, mThumbnailUrl, description, details, mImageOrientation);
        mArtworkDatabaseReference.child(databaseKey).setValue(mArtwork);

        //https://stackoverflow.com/questions/43584244/
        Map<String, Object> map = new HashMap<>();
        map.put(timeStamp, ServerValue.TIMESTAMP);

        //https://stackoverflow.com/questions/38593245
        mArtworkDatabaseReference.child(databaseKey).updateChildren(map);

        mArtworkDatabaseReference.child(databaseKey).child("keywords").setValue(getChipMap());
        progressDialog.dismiss();
        showAudioConfirmationDialog();
        updatePublicationCounter();
        clearInputFields();
    }

    private Map<String, String> getChipMap() {
        // Taken from https://stackoverflow.com/questions/49654537/firebase-querying-liststring-contains-value
        Map<String, String> keywords = new HashMap<>();
        for (int i = 0; i < chipList.size(); i++) {
            keywords.put(chipList.get(i).getText(), "true");
        }
        return keywords;
    }

    private void showAudioConfirmationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(R.string.add_audio_msg);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Bundle bundle = new Bundle();
                bundle.putParcelable(PARCELABLE_KEY, mArtwork);
                switchToAudioFragment(mArtwork.getId());
            }
        });
        builder.setNegativeButton(R.string.add_audio_no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (dialog != null) {
                    dialog.dismiss();
                    returnToProfileScreen(0);
                    Utils.hideKeyboardFrom(getContext(), getView());
                }
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    protected void updateArtworkDb() {
        Map<String, Object> artworkUpdates = new HashMap<>();
        artworkUpdates.put(TITLE_KEY, title);
        artworkUpdates.put(YEAR_KEY, year);
        artworkUpdates.put(AUTHOR_KEY, authorName);
        artworkUpdates.put(WIDTH_KEY, width);
        artworkUpdates.put(HEIGHT_KEY, height);
        artworkUpdates.put(MEDIUM_KEY, medium);
        artworkUpdates.put(DESCRIPTION_KEY, description);
        artworkUpdates.put(DETAILS_KEY, details);
        artworkUpdates.put(IMAGE_ORIENTATION_KEY, mImageOrientation);

        if (mImageUrl != null) {
            artworkUpdates.put(IMAGE_URL_KEY, mImageUrl);
        }

        if (mThumbnailUrl != null) {
            artworkUpdates.put(THUMBNAIL_URL_KEY, mThumbnailUrl);
        }

        artworkViewModel.updateArtwork(mArtwork.getId(), artworkUpdates, getChipMap());
        Toast.makeText(getActivity(), "Publication updated!", Toast.LENGTH_SHORT).show();
    }

    private void clearInputFields() {
        mBinding.inputArtworkInfo.artworkTitleEditText.setText("");
        mBinding.inputArtworkInfo.artworkYearEditText.setText("");
        mBinding.inputArtworkInfo.artworkHeightEditText.setText("");
        mBinding.inputArtworkInfo.artworkWidthEditText.setText("");
        mBinding.inputArtworkInfo.artworkMediumSpinner.setSelection(mSpinnerAdapter.getPosition("Select Medium"));
        mBinding.inputArtworkInfo.artworkDetailsEditText.setText("");
        mBinding.inputArtworkInfo.artworkDescriptionEditText.setText("");
        mBinding.artworkImageView.setImageResource(R.drawable.add_picture_icon_13);
    }

    private void updatePublicationCounter() {
        mUserViewModel.updateCounter(mSignedUserId, mNumberPublications + 1);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void returnCurrentUserFromDb() {
        if (mSignedUserId != null) {
            LiveData<User> userLiveData = mUserViewModel.getUserById(mSignedUserId);
            userLiveData.observe(this, new Observer<User>() {
                @Override
                public void onChanged(@Nullable User user) {
                    if (user != null) {
                        mCurrentUser = user;
                        mCurrentUsername = user.getUsername();
                        mNumberPublications = Integer.parseInt(mCurrentUser.getNumberPublications());
                        setAcceptTermsVisibility();
                        if (!mCurrentUser.getCanPost() && mNumberPublications == 4) {
                            mBinding.verifyTv.setVisibility(View.VISIBLE);
                            mBinding.verify2Tv.setVisibility(View.VISIBLE);

                            if (mBinding.verify2Tv.getVisibility() == View.VISIBLE) {
                                mBinding.verify2Tv.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        sendContactEmail();
                                    }
                                });
                            }

                            mBinding.inputArtworkInfo.saveBtn.setEnabled(false);
                        }
                    }
                }
            });
        }
    }

    private void sendContactEmail() {
        Intent i = new Intent(Intent.ACTION_SENDTO);
        i.setData(Uri.parse("mailto:"));
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"backgrounds.audioguide@gmail.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, getActivity().getResources().getString(R.string.share_email_title, mCurrentUser.getId()));
        i.putExtra(Intent.EXTRA_TEXT, getActivity().getResources().getString(R.string.email_content));

        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_email_client), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mArtwork != null) {
            getActivity().invalidateOptionsMenu();
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.updateBtn);
        MenuItem item2 = menu.findItem(R.id.edit_audio_menu);
        if (mArtwork == null) {
            item.setVisible(false);
            item2.setVisible(false);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.edit_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.updateBtn:
                saveUpdateArtwork();
                return true;
            case R.id.edit_audio_menu:
                switchToAudioFragment(mArtwork.getId());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showProgressDialog() {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage(getString(R.string.saving_message));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void switchToAudioFragment(String artworkId) {
        Bundle bundle = new Bundle();
        bundle.putString(PARCELABLE_KEY, artworkId);
        SaveAudioFragment audioFragment = new SaveAudioFragment();
        audioFragment.setArguments(bundle);

        this.setExitTransition(TransitionInflater.from(getActivity()).inflateTransition(R.transition.audio_slide));
        audioFragment.setEnterTransition(TransitionInflater.from(getActivity()).inflateTransition(R.transition.audio_slide));
        ((MainActivity) getActivity()).replaceFragment(audioFragment, null);
    }
}