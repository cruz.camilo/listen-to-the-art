package com.camcruzt.backgrounds.listentotheart.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.camcruzt.backgrounds.listentotheart.R;

import java.util.List;

public class PrivacyRecyclerAdapter extends RecyclerView.Adapter<PrivacyRecyclerAdapter.PrivacyViewHolder> {

    private Context context;
    private List<String> mTitlesList;
    private List<String> mContentList;

    public PrivacyRecyclerAdapter(Context context, List<String> titles, List<String> content) {
        this.context = context;
        this.mTitlesList = titles;
        this.mContentList = content;
    }

    class PrivacyViewHolder extends RecyclerView.ViewHolder{
        TextView title;
        TextView content;
        public PrivacyViewHolder(@NonNull View itemView) {
            super(itemView);
            title =  (TextView) itemView.findViewById(R.id.privacy_title);
            content = (TextView) itemView.findViewById(R.id.privacy_content_jv);
        }
    }

    @NonNull
    @Override
    public PrivacyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        int itemPrivacy = R.layout.item_privacy;
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(itemPrivacy, viewGroup, false);
        PrivacyViewHolder viewHolder = new PrivacyViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PrivacyViewHolder holder, int position) {
        if(position==0 || position==mContentList.size()-1){
            holder.title.setVisibility(View.GONE);
        } else {
            holder.title.setText(mTitlesList.get(position-1));
        }

        holder.content.setText(mContentList.get(position));
    }

    @Override
    public int getItemCount() {
        return mContentList.size();
    }

}