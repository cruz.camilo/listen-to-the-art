### Backgrounds

[Backgrounds](https://play.google.com/store/apps/details?id=com.camcruzt.backgrounds.listentotheart) allows artists to present their visual artworks providing an audio 
description about their works. Users can zoom in and out the pictures 
to fully appreciate the details of the artwork while listening to the description.

This app uses [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/)
and [Firebase](https://firebase.google.com/) components such as Firebase UI, 
Firebase Realtime Database and Firebase Storage.

[Backgrounds](https://play.google.com/store/apps/details?id=com.camcruzt.backgrounds.listentotheart) includes also a Widget that shows the last displayed artwork.


### Libraries

* [ExoPlayer](https://github.com/google/ExoPlayer)
* [Glide](https://github.com/bumptech/glide)
* [BigImageViewer](https://github.com/Piasy/BigImageViewer)
* [ExpandableTextView](https://github.com/Manabu-GT/ExpandableTextView)
* [Country Code Picker Library](https://github.com/hbb20/CountryCodePickerProject)
* [ChipView](https://github.com/Plumillon/ChipView)

### Screenshots

<img src="https://gitlab.com/cruz.camilo/listen-to-the-art/raw/master/screenshots/1.home.png" width="360" height="643">
<img src="https://gitlab.com/cruz.camilo/listen-to-the-art/raw/master/screenshots/2.large%20screen.png" width="360" height="643">
<img src="https://gitlab.com/cruz.camilo/listen-to-the-art/raw/master/screenshots/3.details.png" width="360" height="643">
<img src="https://gitlab.com/cruz.camilo/listen-to-the-art/raw/master/screenshots/5.widget.png" width="360" height="643">
<img src="https://gitlab.com/cruz.camilo/listen-to-the-art/raw/master/screenshots/4.horizontal%20full%20screen.png">